﻿using UnityEngine;
using System.Collections;
namespace S3{
	public class BloodTrail : MonoBehaviour {

public Renderer a;
public Renderer b;
public Renderer c;
public Renderer d;
public Renderer e;


		void SetInitialReferences(){
			a.enabled = false;
			b.enabled = false;
			c.enabled = false;
			d.enabled = false;
			e.enabled = false;
		}

		void Start()
		{
			SetInitialReferences ();
		}

		public void toggle()
		{
			a.enabled = !a.enabled ;
			b.enabled = !b.enabled ;
			c.enabled = !c.enabled;
			d.enabled = !d.enabled;
			e.enabled = !e.enabled;
		}
			
	}
}
