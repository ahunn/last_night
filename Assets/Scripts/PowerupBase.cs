﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupBase : MonoBehaviour {

    public delegate void PowerUpEventHandler(int val);
    public event PowerUpEventHandler EventHealthPowerup;
    public event PowerUpEventHandler EventAttackPowerup;

    public void CallEventHealthPowerup(int val)
    {
        if (EventHealthPowerup != null)
            EventHealthPowerup(val);
    }

    public void CallEventAttackPowerup(int val)
    {
        if (EventAttackPowerup != null)
            EventAttackPowerup(val);
    }
}
