﻿using UnityEngine;
using System.Collections;

public class HealthPack : MonoBehaviour
{
    [Header("Health Pack Settings")]
    public int healthAmount;

    void OnEnable()
    {
        GetComponent<ItemManager>().EventObjectPickup += TakeHealth;
    }

    void OnDisable()
    {
        GetComponent<ItemManager>().EventObjectPickup -= TakeHealth;
    }

    public void TakeHealth()
    {

        PlayerManager.instance.CallEventPlayerHealthIncrease(healthAmount);
        Destroy(gameObject);
    }
}
