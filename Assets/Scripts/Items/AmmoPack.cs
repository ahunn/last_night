﻿using UnityEngine;
using System.Collections;

public class AmmoPack : MonoBehaviour
{
    [Header("Ammo Pack Settings")]
    public string ammoName;
    public int ammoQuantity;

    void OnEnable()
    {
        GetComponent<ItemManager>().EventObjectPickup += TakeAmmo;
    }

    void OnDisable()
    {
        GetComponent<ItemManager>().EventObjectPickup -= TakeAmmo;
    }

    public void TakeAmmo()
    {
        PlayerManager.instance.CallEventPickedUpAmmo(ammoName,ammoQuantity);
        Destroy(gameObject);
    }
}
