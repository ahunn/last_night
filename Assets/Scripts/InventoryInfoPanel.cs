﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class InventoryInfoPanel : MonoBehaviour
{
    private Dictionary<int, string> description = new Dictionary<int, string>();

    //JSON 
    private string path;
    private string jsonString;


    void SetJSON()
    {
        path = Application.streamingAssetsPath + "/info.json";
        jsonString = File.ReadAllText(path);
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    //Builds dictionary of stories from JSON file. Not needed to be like this
    private void BuildDictionary()
    {
        InventoryID idJSON = JsonUtility.FromJson<InventoryID>(jsonString);
        //description[0] = idJSON.
    }

}
