﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBase : MonoBehaviour {
    public delegate void GeneralEventHandler();
    public event GeneralEventHandler EventObjectThrow;
    public event GeneralEventHandler EventObjectPickup;
    public delegate void PickupActionEventHandler(Transform item);
    public event PickupActionEventHandler EventPickupAction;

    private Transform myTransform;
    private Rigidbody myRigidbody;
    private Collider itemCollider;


    public virtual void SetInitalReferences()
    {
        myTransform = transform;
        myRigidbody = GetComponent<Rigidbody>();
        itemCollider = GetComponent<Collider>();
    }

    public Transform ReturnTransform(){ return myTransform; }

    public Rigidbody ReturnRigidbody() { return myRigidbody; }

    public Collider ReturnCollider() {return itemCollider; }
     
    #region Events
    public void CallEventObjectThrow()
    {
        if (EventObjectThrow != null)
            EventObjectThrow();

        PlayerManager.instance.CallEventHandsEmpty();
        PlayerManager.instance.CallEventInventoryChanged();
    }
    public void CallEventObjectPickup()
    {
        if (EventObjectPickup != null)
            EventObjectPickup();

        PlayerManager.instance.CallEventInventoryChanged();
    }
    public void CallEventPickupAction(Transform item)
    {
        if (EventPickupAction != null)
            EventPickupAction(item);

    }
    #endregion


}
