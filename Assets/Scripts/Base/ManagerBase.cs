﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerBase : MonoBehaviour {

    protected SceneLoader sceneLoader;
    //public delegate void ButtonPressed();
    public delegate void EventTriggered();
    public delegate void PlayerEvents();
    


	void Awake () {
        if (this.GetComponent<SceneLoader>() != null)
            sceneLoader = this.GetComponent<SceneLoader>();
        else
          sceneLoader = this.gameObject.AddComponent<SceneLoader>();
    
	}

    public virtual void GoToMainMenu() {sceneLoader.LoadScene("MainMenu");}

    public virtual void StartSurvivalMode() { sceneLoader.LoadScene("SurvivalMode"); }

    public virtual void StartMainGame() { sceneLoader.LoadScene("Prolog"); }

    public virtual void RestartLevel(){Application.LoadLevel(Application.loadedLevel);}

    public virtual void GoToTesting() { sceneLoader.LoadScene("TestingMode"); }

    public virtual void ExitGame() { Application.Quit(); }


}
