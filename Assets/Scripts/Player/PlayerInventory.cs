﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;

 public class PlayerInventory : MonoBehaviour 
{
    public Transform playerInventoryTransform;
    public Transform inventoryParentTransform;
    public GameObject inventoryButton;
    private float timeToPlaceInHands = 0.1f;
    private Transform currentlyHeldItem;
    private int counter;
    private string buttonText;
    private List<Transform> listInventory = new List<Transform>();

    public static PlayerInventory instance;

    void Awake()
    {
        instance = this;
     }

    void OnEnable()
    {
        UpdateInventoryListAndUI();
        CheckIfHandsEmpty();
        DeactivateAllInventoryItems();

        PlayerManager.instance.EventInventoryChanged += UpdateInventoryListAndUI;
        PlayerManager.instance.EventInventoryChanged += CheckIfHandsEmpty;
        PlayerManager.instance.EventHandsEmpty += ClearHands;
        }

    void OnDisable()
    {
        PlayerManager.instance.EventInventoryChanged -= UpdateInventoryListAndUI;
        PlayerManager.instance.EventInventoryChanged -= CheckIfHandsEmpty;
        PlayerManager.instance.EventHandsEmpty -= ClearHands;
    }

    void UpdateInventoryListAndUI()
    {
        counter = 0;
        listInventory.Clear();
        listInventory.TrimExcess();

        ClearInventoryUI();

        foreach (Transform child in playerInventoryTransform)
            if (child.CompareTag("Item"))
            {
                if (child.GetComponent<AmmoPack>() != null)
                    continue;
                if (child.GetComponent<HealthPack>() != null)
                    continue;
                listInventory.Add(child);
                GameObject go = Instantiate(inventoryButton) as GameObject;
                buttonText = child.name;
                go.GetComponentInChildren<TextMeshProUGUI>().text = buttonText;
                int index = counter;
                go.GetComponent<Button>().onClick.AddListener(delegate { ActivateInventoryItem(index); });
                go.GetComponent<Button>().onClick.AddListener(() =>GameManager.instance.ToggleInventory());
                go.transform.SetParent(inventoryParentTransform, false);
                counter++;
            }
     }

    void CheckIfHandsEmpty()
    {
        if (currentlyHeldItem == null && listInventory.Count > 0)
            StartCoroutine(PlaceItemInHands(listInventory[listInventory.Count - 1]));
    }

    void ClearHands()
    {
        currentlyHeldItem = null;
    }

    void ClearInventoryUI()
    {
        foreach (Transform child in inventoryParentTransform)
            Destroy(child.gameObject);
    }

    public void ActivateInventoryItem(int inventoryIndex)
    {
        DeactivateAllInventoryItems();
        StartCoroutine(PlaceItemInHands(listInventory[inventoryIndex]));
    }

    void DeactivateAllInventoryItems()
    {
        foreach (Transform child in playerInventoryTransform)
                if (child.CompareTag("Item"))
                    child.gameObject.SetActive(false);
    }

    IEnumerator PlaceItemInHands(Transform itemTransform)
    {
        yield return new WaitForSeconds(timeToPlaceInHands);
        currentlyHeldItem = itemTransform;
        currentlyHeldItem.gameObject.SetActive(true);
        
    }
}

