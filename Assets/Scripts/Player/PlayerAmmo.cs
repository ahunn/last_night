﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerAmmo : MonoBehaviour
{
    [System.Serializable]
    public class AmmoTypes
    {
        public string ammoName;
        public int ammoMaxQuantity;
        public int ammoCurrentCarried;

        public AmmoTypes(string aName, int aMaxQuantity, int aCurrentCarried)
        {
            ammoName = aName;
            ammoMaxQuantity = aMaxQuantity;
            ammoCurrentCarried = aCurrentCarried;
         }
     }

    public List<AmmoTypes> typesOfAmmunition = new List<AmmoTypes>();

    void OnEnable()
    {
        PlayerManager.instance.EventPickedUpAmmo += PickedUpAmmo;
    }

    void OnDisable()
    {
        PlayerManager.instance.EventPickedUpAmmo -= PickedUpAmmo;
    }

    public void PickedUpAmmo(string ammoName, int quantity)
    {
        for (int i = 0; i < typesOfAmmunition.Count; i++)
            if (typesOfAmmunition[i].ammoName == ammoName)
            {
                typesOfAmmunition[i].ammoCurrentCarried += quantity;

                if (typesOfAmmunition[i].ammoCurrentCarried > typesOfAmmunition[i].ammoMaxQuantity)
                        typesOfAmmunition[i].ammoCurrentCarried = typesOfAmmunition[i].ammoMaxQuantity;
   
                    PlayerManager.instance.CallEventAmmoChanged();
                    break;
                }
    }
}

