﻿using UnityEngine;
using System.Collections;

public class SceneLoader : MonoBehaviour {

	private string levelToLoad;
	public GameObject loadingBar;
	public AsyncOperation theload; 
	private bool isLoading;


	public enum Pillar{create, discover, compare,superDuty,SuperDutyCreate};
	private Pillar scene;

	void Awake(){
		loadingBar.SetActive(false);
	}


	void Update()
	{
		loadingBar.transform.Rotate(0,0,-360 * Time.deltaTime);
		if(isLoading)
		{
			if(theload.isDone)
			{
				theload.allowSceneActivation = true;
			}
		}
	}

	public void LoadScene(string scene)
	{
		loadingBar.SetActive(true);
		levelToLoad = scene;
		theload = Application.LoadLevelAsync(levelToLoad);

		isLoading = true;
	}


}
