﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnableObject : MonoBehaviour
{
    public GameObject ammoPack;
    public GameObject healthPack;
    public GameObject enemyPrefab;
    public GameObject shotgunPrefab;
    public GameObject knifePrefab;
    public GameObject pistolPrefab;
    public GameObject axePrefab;

    private GameObject[] gameObjects;

    public bool isUserActivated = false;
    public float time;

    public enum Items { AMMO, HEALTH, ENEMY, SHOTGUN, KNIFE, PISTOL, AXE, MAX };

    void Awake()
    {
         gameObjects = new GameObject[] { ammoPack, healthPack, enemyPrefab, shotgunPrefab, knifePrefab, pistolPrefab, axePrefab };
        //gameObjects[(int)Items.AMMO] = ammoPack;
        //gameObjects[(int)Items.HEALTH] = healthPack;
        //gameObjects[(int)Items.ENEMY] = enemyPrefab;
        //gameObjects[(int)Items.SHOTGUN] = shotgunPrefab;
        //gameObjects[(int)Items.KNIFE] = knifePrefab;
        //gameObjects[(int)Items.PISTOL] = pistolPrefab;
        //gameObjects[(int)Items.AXE] = axePrefab;
        

    }

    private void OnEnable()
    {
        SpawnItem();
    }

    public void RandomItem()
    {
        
        int rand = Random.Range(0, (int)Items.MAX);
        GameObject randomItem= Instantiate(gameObjects[rand], this.gameObject.transform) as GameObject;
    }

    public void ForceItem(Items item)
    {
        GameObject randomItem= Instantiate(gameObjects[(int)item], this.gameObject.transform);
    }

    public void SpawnItem()
    {
        if (isUserActivated)
            InvokeRepeating("RandomItem", 3, time);
    }




}
