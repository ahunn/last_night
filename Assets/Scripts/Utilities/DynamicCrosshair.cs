﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class DynamicCrosshair : MonoBehaviour {

    enum CrosshairLocations { TOP,BOTTOM,LEFT,RIGHT};
    private Image[] crosshairPrefabs;
    private float value = 15f;

    private void Awake()
    {
        crosshairPrefabs = GetComponentsInChildren<Image>();
    }

    private void Start()
    {
        foreach (Image x in crosshairPrefabs)
            x.color = Color.white;
    }

    
    public void SpeedChange(float speed)
    {
        Sequence speedChange = DOTween.Sequence();
        speedChange.Join(crosshairPrefabs[(int)CrosshairLocations.TOP].transform.DOLocalMoveY(value, 0.1f))
                              .Join(crosshairPrefabs[(int)CrosshairLocations.BOTTOM].transform.DOLocalMoveY((value * -1), 0.1f))
                              .Join(crosshairPrefabs[(int)CrosshairLocations.LEFT].transform.DOLocalMoveX(value, 0.1f))
                              .Join(crosshairPrefabs[(int)CrosshairLocations.RIGHT].transform.DOLocalMoveX((value * -1), 0.1f));

        if ((int)speed < 1)
            value = 15f;
        else if((int)speed >1 && (int)speed < 4)
            value = 20f;
        else
            value = 25f;

        speedChange.Play();
    }

    public void EnemyInSight(bool val = false)
    {
        Sequence colorChange = DOTween.Sequence();
        colorChange.Join(crosshairPrefabs[(int)CrosshairLocations.TOP].DOColor(val ? Color.red : Color.white, 0.1f))
                            .Join(crosshairPrefabs[(int)CrosshairLocations.BOTTOM].DOColor(val ? Color.red : Color.white, 0.1f))
                            .Join(crosshairPrefabs[(int)CrosshairLocations.LEFT].DOColor(val ? Color.red : Color.white, 0.1f))
                            .Join(crosshairPrefabs[(int)CrosshairLocations.RIGHT].DOColor(val ? Color.red : Color.white, 0.1f));
        colorChange.Play();
    }
}
