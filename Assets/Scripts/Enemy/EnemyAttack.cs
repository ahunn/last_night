﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    private Transform attackTarget;
    private float nextAttack;
    private EnemyManager manager;

    [Header("Attack Stats")]
    public float attackRate = 1;            
    public float attackRange = 3.5f;    
    public int attackDamage = 10;           

    void OnEnable()
    {
        SetInitialReference();
        manager.EventEnemyDie += DisableAttack;
        manager.EventEnemySetNavTarget += SetAttackTarget;
    }

    void OnDisable()
    {
        manager.EventEnemyDie -= DisableAttack;
        manager.EventEnemySetNavTarget -= SetAttackTarget;
    }
    void SetInitialReference()
    {
        manager = GetComponent<EnemyManager>();
    }

    void Update()
    {
        TryToAttack();
    }

    void SetAttackTarget(Transform targetTransform){ attackTarget = targetTransform; }

    void TryToAttack()
    {
        if (attackTarget != null)
            if (Time.time > nextAttack)
            {
                nextAttack = Time.time + attackRate;
                if (Vector3.Distance(manager.myTransform.position, attackTarget.position) <= attackRange)
                {
                    Vector3 lookAtVector = new Vector3(attackTarget.position.x, manager.myTransform.position.y, attackTarget.position.z);
                    manager.myTransform.LookAt(lookAtVector);
                    manager.CallEventEnemyAttack();
                    manager.isOnRoute = false;
                }
            }
    }

    //Called by animation
    public void OnEnemyAttack()
    {
        if (attackTarget != null)
            if (Vector3.Distance(manager.myTransform.position, attackTarget.position) <= attackRange &&
                    attackTarget.GetComponent<PlayerManager>() != null)
            {
                Vector3 toOther = attackTarget.position - manager.myTransform.position;
                if (Vector3.Dot(toOther, manager.myTransform.forward) > 0.5f)
                    PlayerManager.instance.CallEventPlayerHealthDeduction(attackDamage);
            }
    }

    //Disables attack when roaming
    void DisableAttack(){ this.enabled = false; }
}

