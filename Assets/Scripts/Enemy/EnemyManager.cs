﻿using UnityEngine;
using System.Collections;

public class EnemyManager : MonoBehaviour
{
    public Transform playerTransform;
    public Transform myTransform;
    public bool isOnRoute;
    public bool isNavPaused;

    private Animator myAnimator;

    public delegate void GeneralEventHandler();
    public event GeneralEventHandler EventEnemyDie;
    public event GeneralEventHandler EventEnemyWalking;
    public event GeneralEventHandler EventEnemyReachedNavTarget;
    public event GeneralEventHandler EventEnemyAttack;
    public event GeneralEventHandler EventEnemyLostTarget;

    public delegate void HealthEventHandler(int health);
    public event HealthEventHandler EventEnemyDeductHealth;

    public delegate void NavTargetEventHandler(Transform targetTransform);
    public event NavTargetEventHandler EventEnemySetNavTarget;

    [Header("Health and Damage Taken")]
    public int enemyHealth = 100;
    public int damageMultiplier = 1;
    public bool removeCollider = false;

    private Collider myCollider;
    private Rigidbody myRigidbody;

    public GameObject rig;


    private void OnEnable()
    {
        AnimationEventEnable();
        EventEnemyDeductHealth += DeductHealth;
        EventEnemyDie += RemoveColliderAndRigidbody;
        EventEnemyDie += ActivateRagdoll;

    }

    private void OnDisable()
    {
        AnimationEventDisable();
        EventEnemyDeductHealth -= DeductHealth;
        EventEnemyDie -= RemoveColliderAndRigidbody;
        EventEnemyDie -= ActivateRagdoll;

    }

    void Awake()
    {
        if (GetComponent<Animator>() != null)
            myAnimator = GetComponent<Animator>();
        myTransform = transform;

        if (GetComponent<Collider>() != null)
            myCollider = GetComponent<Collider>();

        if (GetComponent<Rigidbody>() != null)
            myRigidbody = GetComponent<Rigidbody>();
    }



    #region Animation

    void AnimationEventEnable()
    {
        EventEnemyDie += EnemyDeath;
        EventEnemyWalking += SetToPrusuit;
        EventEnemyReachedNavTarget += SetToIdle;
        EventEnemyAttack += SetToAttack;
        EventEnemyDeductHealth += SetToStruck;
    }

    void AnimationEventDisable()
    {
        EventEnemyDie -= EnemyDeath;
        EventEnemyWalking -= SetToPrusuit;
        EventEnemyReachedNavTarget -= SetToIdle;
        EventEnemyAttack -= SetToAttack;
        EventEnemyDeductHealth -= SetToStruck;
    }

    void EnemyDeath()
    {
        myAnimator.enabled = false;
    }
   
    void SetToPrusuit()
    {
        if (myAnimator.enabled)
            myAnimator.SetBool("isPursuing", true);
    }

    void SetToIdle()
    {
        if (myAnimator.enabled)
            myAnimator.SetBool("isPursuing", false);
    }

    void SetToAttack()
    {
        if (myAnimator.enabled)
            myAnimator.SetTrigger("Attack") ;
    }

    void SetToStruck(int dummy)
    {
        if (myAnimator.enabled)
            myAnimator.SetTrigger("Struck");
    }


    #endregion

    #region Health

    void DeductHealth(int val)
    {
        enemyHealth -= val;
        if(enemyHealth <= 0)
        {
            enemyHealth = 0;
            CallEventEnemyDie();
            Destroy(gameObject, Random.Range(5, 8));
        }

    }

    public void ProcessDamage(int damage)
    {
        CallEventEnemyDeductHealth(damage * damageMultiplier);
    }

    #endregion

    void ActivateRagdoll()
    {
        if (myRigidbody != null)
        {
            myRigidbody.isKinematic = false;
            myRigidbody.useGravity = true;
        }
        if (myCollider != null)
        {
            myCollider.isTrigger = false;
            myCollider.enabled = true;
        }
    }

    void RemoveColliderAndRigidbody()
    {

            //Should be hitbox
           GetComponentInChildren<Collider>().enabled = false;
 
        foreach(Collider x in rig.GetComponentsInChildren<Collider>())
        {
            x.gameObject.GetComponent<Collider>().enabled = true;
        }
        foreach (Rigidbody x in rig.GetComponentsInChildren<Rigidbody>())
        {
            x.useGravity = true;
            x.isKinematic = false;
            x.mass = 100;
        }

        if (GetComponent<Rigidbody>() != null)
        {
            GetComponent<Rigidbody>().isKinematic = false;
            GetComponent<Rigidbody>().useGravity = true;
        }
            

    }

    

    #region Events
    public void CallEventEnemyDeductHealth(int health)
    {
        if (EventEnemyDeductHealth != null)
            EventEnemyDeductHealth(health);
    }
    public void CallEventEnemySetNavTarget(Transform targTransform)
    {
        if (EventEnemySetNavTarget != null)
            EventEnemySetNavTarget(targTransform);
        
        playerTransform = targTransform;
    }
    public void CallEventEnemyDie()
    {
        if (EventEnemyDie != null)
            EventEnemyDie();
    }
    public void CallEventEnemyWalking()
    {
        if (EventEnemyWalking != null)
            EventEnemyWalking();
    }
    public void CallEventEnemyReachedNavTarget()
    {
        if (EventEnemyReachedNavTarget != null)
            EventEnemyReachedNavTarget();
    }
    public void CallEventEnemyAttack()
    {
        if (EventEnemyAttack != null)
            EventEnemyAttack();
    }
    public void CallEventEnemyLostTarget()
    {
        if (EventEnemyLostTarget != null)
            EventEnemyLostTarget();
   
        playerTransform = null;
    }
    #endregion



}
