﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCollisionField : MonoBehaviour {

    private Rigidbody rigidBodyStrikingMe;

    private int damageToApply;
    private float damageFactor = 0.1f;  //Damage factor given mass and speed reached

    public float massRequirement = 50;  //Mass of thrown object needed to damage
    public float speedRequirement = 5;  //Speed of object needed to damage

    private EnemyManager manager;

    void SetInitialReference()
    {
        manager = GetComponent<EnemyManager>();
    }

    void OnEnable()
    {
        SetInitialReference();
        manager.EventEnemyDie += DisableThis;
    }

    void OnDisable()
    {
        manager.EventEnemyDie -= DisableThis;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Rigidbody>() != null)
        {

            rigidBodyStrikingMe = other.GetComponent<Rigidbody>();

            if (rigidBodyStrikingMe.mass >= massRequirement &&
                    rigidBodyStrikingMe.velocity.sqrMagnitude > speedRequirement * speedRequirement)
            {

                //Given speed and mass, calculate damage inflicted by player and deduct health from enemy
                damageToApply = (int)(damageFactor * rigidBodyStrikingMe.mass * rigidBodyStrikingMe.velocity.magnitude);
                manager.CallEventEnemyDeductHealth(damageToApply);
            }
        }
    }

    void DisableThis()
    {
        gameObject.SetActive(false);
    }
}
