﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDetection : MonoBehaviour {
    [Header("Detection Stats")]
    public Transform head;
    public LayerMask playerLayer;
    public LayerMask sightLayer;

    private RaycastHit hit;
    private float checkRate;
    private float nextCheck;
    private float detectRadius = 360; //80
    private EnemyManager manager;

    void OnEnable()
    {
        SetInitialReference();
        if (head == null)
            head = manager.myTransform;

        checkRate = Random.Range(0.8f, 1f);
        manager.EventEnemyDie += DisableDetection;
    }

    void OnDisable()
    {
        manager.EventEnemyDie -= DisableDetection;
    }
    void SetInitialReference() {
        manager = GetComponent<EnemyManager>();
    }

    void Update()
    {
        CarryOutDetection();
    }

    /**Simulates enemy field of vision**/
    void CarryOutDetection()
    {
        if (Time.time > nextCheck)
        {
            nextCheck = Time.time + checkRate;

            Collider[] colliders = Physics.OverlapSphere(manager.myTransform.position, detectRadius, playerLayer);

            if (colliders.Length > 0)
                foreach (Collider potentialTargetCollider in colliders)
                    if (potentialTargetCollider.CompareTag(GameManager.instance.ReturnPlayerTag()))
                        if (CanPotentialTargetBeSeen(potentialTargetCollider.transform))
                            break;
            else
                manager.CallEventEnemyLostTarget();
        }
    }

    /**If player can be seen, call set nav target event, else, call lost target event**/
    bool CanPotentialTargetBeSeen(Transform potentialTarget)
    {
        if (Physics.Linecast(head.position, potentialTarget.position, out hit, sightLayer))
        {
            if (hit.transform == potentialTarget)
            {
                manager.playerTransform = potentialTarget;
                manager.CallEventEnemySetNavTarget(potentialTarget);
                return true;
            }
            else
            {
                manager.playerTransform = null;
                manager.CallEventEnemyLostTarget();
                return false;
            }
        }
        else
        {
            manager.playerTransform = null;
            manager.CallEventEnemyLostTarget();
            return false;
        }
    }

    void DisableDetection(){ this.enabled = false; }
}

