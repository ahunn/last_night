﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyNavigation : MonoBehaviour{

    private UnityEngine.AI.NavMeshAgent myNavMeshAgent;
    private EnemyManager manager;
    private float checkRate;
    private float checkRateWander;
    private float checkRateDestination;
    private float nextCheck;
    private float nextCheckWander;
    private float nextCheckDestination;
    private float pauseTime = 1;

    private float wanderRange = 10;
    private UnityEngine.AI.NavMeshHit navHit;
    private Vector3 wanderTarget;

    void OnEnable()
    {
        SetInitialReferences();
        manager.EventEnemyDie += DisableNavigation;
        manager.EventEnemyDeductHealth += PauseNavMeshAgent;
        InvokeRepeating("OptimizedNavigation", 0, 0.3f);
    }

    void OnDisable()
    {
        manager.EventEnemyDie -= DisableNavigation;
        manager.EventEnemyDeductHealth -= PauseNavMeshAgent;
        CancelInvoke("OptimizedNavigation");
    }

    void Update()
    {

    }

    void OptimizedNavigation()
    {
        //Prusuing Target
        if (Time.time > nextCheck)
        {
            nextCheck = Time.time + checkRate;
            TryToChaseTarget();
            CheckIfIShouldWander();
            CheckIfDestinationReached();
        }

        //if (Time.time > nextCheckWander)
        //{
        //    nextCheckWander = Time.time + checkRateWander;
        //    CheckIfIShouldWander();
        //}

        //if (Time.time > nextCheckDestination)
        //{
        //    nextCheckDestination = Time.time + checkRate;
        //    CheckIfDestinationReached();
        //}
    }

    void SetInitialReferences()
    {
        manager = GetComponent<EnemyManager>();
        if (GetComponent<UnityEngine.AI.NavMeshAgent>() != null)
            myNavMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        checkRate = Random.Range(0.1f, 0.2f);
        checkRateWander = Random.Range(0.3f, 0.4f);
        checkRateDestination = Random.Range(0.3f, 0.4f);
    }

    #region Nav Prusue
    void TryToChaseTarget()
    {
        if (manager.playerTransform != null && myNavMeshAgent != null && !manager.isNavPaused)
        {
            myNavMeshAgent.SetDestination(manager.playerTransform.position);

            if (myNavMeshAgent.remainingDistance > myNavMeshAgent.stoppingDistance)
            {
                manager.CallEventEnemyWalking();
                manager.isOnRoute = true;
            }
        }
    }
    #endregion

    #region Nav Pause
    void PauseNavMeshAgent(int dummy)
    {
        if (myNavMeshAgent != null)
            if (myNavMeshAgent.enabled)
            {
                myNavMeshAgent.ResetPath();
                manager.isNavPaused = true;
                StartCoroutine(RestartNavMeshAgent());
            }
    }
    IEnumerator RestartNavMeshAgent()
    {
        yield return new WaitForSeconds(pauseTime);
        manager.isNavPaused = false;
    }

    
    #endregion

    #region Nav Wander
    void CheckIfIShouldWander()
    {
        if (manager.playerTransform == null && !manager.isOnRoute && !manager.isNavPaused)
            if (RandomWanderTarget(manager.myTransform.position, wanderRange, out wanderTarget))
            {
                myNavMeshAgent.SetDestination(wanderTarget);
                manager.isOnRoute = true;
                manager.CallEventEnemyWalking();
            }
    }

    bool RandomWanderTarget(Vector3 center, float range, out Vector3 result)
    {
        Vector3 randomPoint = center + Random.insideUnitSphere * wanderRange;
        if (UnityEngine.AI.NavMesh.SamplePosition(randomPoint, out navHit, 1.0f, UnityEngine.AI.NavMesh.AllAreas))
        {
            result = navHit.position;
            return true;
        }
        else
        {
            result = center;
            return false;
        }
    }
    #endregion

    #region Destination Reached
    void CheckIfDestinationReached()
    {
        if (manager.isOnRoute)
            if (myNavMeshAgent.remainingDistance < myNavMeshAgent.stoppingDistance)
            {
                manager.isOnRoute = false;
                manager.CallEventEnemyReachedNavTarget();
            }
    }
    #endregion

    void DisableNavigation()
    {
        myNavMeshAgent.enabled = false;
        this.enabled = false;
        StopAllCoroutines();
    }

}
