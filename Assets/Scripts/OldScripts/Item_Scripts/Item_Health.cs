﻿using UnityEngine;
using System.Collections;

namespace S3
{
	public class Item_Health: MonoBehaviour
	{
		private Item_Master itemMaster;
		private GameObject playerGo;
		public int amount;
		public bool isTriggerPickup;
		//private AudioSource sound;
		//public AudioClip clip;

		void OnEnable()
		{
			SetInitialReferences();
			itemMaster.EventObjectPickup += TakeHealth;
		}
		void OnDisable()
		{
			itemMaster.EventObjectPickup -= TakeHealth;
		}
		void Start()
		{
			SetInitialReferences();
		}
		void SetInitialReferences()
		{
			itemMaster = GetComponent<Item_Master>();
			playerGo = GameManager_References._player;
			//sound = GetComponent<AudioSource> ();
			//sound.clip = clip;

			if(isTriggerPickup)
			{
				if(GetComponent<Collider>()!=null)
				{
					GetComponent<Collider>().isTrigger = true;
				}

				if(GetComponent<Rigidbody>() != null)
				{
					GetComponent<Rigidbody>().isKinematic = true;
				}
			}
		}
		void OnTriggerEnter(Collider other)
		{
			if(other.CompareTag(GameManager_References._playerTag)&&isTriggerPickup)
			{
				//sound.Play ();
				TakeHealth();
			}
		}
		void TakeHealth()
		{
			playerGo.GetComponent<Player_Master>().CallEventPlayerHealthIncrease(amount);
			Destroy(gameObject);
		}
	}
}
