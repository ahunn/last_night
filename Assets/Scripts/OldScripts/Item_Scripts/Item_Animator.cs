using UnityEngine;
using System.Collections;

namespace S3
{
	public class Item_Animator: MonoBehaviour
	{
		private Item_Master itemMaster;
		public Animator myAnimator;

		void OnEnable()
		{
			SetInitialReferences();
			itemMaster.EventObjectThrow += DisableMyAnimator;
		  itemMaster.EventObjectPickup += EnableMyAnimaor;
		}
		void OnDisable()
		{
			itemMaster.EventObjectThrow -= DisableMyAnimator;
		  itemMaster.EventObjectPickup -= EnableMyAnimaor;
		}
		void SetInitialReferences()
		{
			itemMaster = GetComponent<Item_Master>();
		}
		void EnableMyAnimaor()
		{
			if(myAnimator != null)
			{
				myAnimator.enabled = true;
			}
		}
		void DisableMyAnimator()
		{
			if(myAnimator != null)
			{
				myAnimator.enabled = false;
			}
		}
	}
}
