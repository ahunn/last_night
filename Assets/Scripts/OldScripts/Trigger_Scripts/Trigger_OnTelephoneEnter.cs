using UnityEngine;
using System.Collections;

namespace S3
{

	public class Trigger_OnTelephoneEnter: MonoBehaviour
	{
		//private Trigger_Master triggerMaster;
		public float defaultVolume = 5;
		public float defVol = 1;
		public string Interact;
		public Light messageLight;
		public Renderer redOrb;
		public Renderer greenOrb;

		public AudioClip radioSound;
	 	public AudioClip distortSound;
		private GameObject temp;
		// 	private Collider myCollider;
		private bool alreadyPlayed = false;
		private bool distortPlay = false;
		private Collider myCollider;

		private bool inTrigger = false;

		void OnEnable()
		{
			SetInitialReferences();
			myCollider.enabled = false;
			messageLight.intensity = 0f;
			redOrb.enabled = false;
			greenOrb.enabled = false;
			//PlaySounds();	//Default Sound No Trigger
		}
		void OnDisable()
		{
		}

		void Update()
		{
			if(inTrigger){
	 			if(Input.GetButtonDown(Interact)){
					messageLight.color = new Color(1f, (60f/255f), (60f/255f));
					redOrb.enabled = true;
					greenOrb.enabled = false;
					if(!alreadyPlayed)
					{
						AudioSource.PlayClipAtPoint(distortSound,(transform.position),defaultVolume);
						alreadyPlayed = true;
					}
	 		}
		}
	}
		void SetInitialReferences()
		{
			//triggerMaster = GetComponent<Trigger_Master>();

			if(GetComponent<Collider>() != null)
			{
				myCollider = GetComponent<Collider>();
			}
		}
		void PlaySounds()
		{
				if(radioSound != null)
				{
					//defaultVolume = 5;
					AudioSource.PlayClipAtPoint(radioSound,(transform.position),defaultVolume);
				}
			}

			// void PlaySounds2()
			// {
			// 		if(!shouldPlay)
			// 		{
			// 			defaultVolume = 5;
			// 			AudioSource.PlayClipAtPoint(distortSound,(transform.position),defaultVolume);
			// 			shouldPlay = true;
			// 		}
			// 	}




		public void isActive(int level)
		{
			if(level == 1)
			{

					PlaySounds();
					messageLight.intensity = 0.48f;
					messageLight.color = new Color((16f/225f), (137f/225f), (77f/255f));
					greenOrb.enabled = true;

			}
			else if(level == 2)
			{
				temp = GameObject.Find("One shot audio");
				Destroy(temp);
				myCollider.enabled = true;



			}

			// if(level < 2)
			// {
			//
			// 	//shouldPlay = false;
			// 	//this.enabled = false;
			// 	myCollider.enabled = false;
			// }
			// else{
			// 	//shouldPlay = true;
			// 	defaultVolume = 5;
			// 	myCollider.enabled = true;
			// 	PlaySounds2();
			// 	// if(shouldPlay == true)
			// 	// {
			// 	// 	AudioSource.PlayClipAtPoint(distortSound,transform.position,defVol);
			// 	// 	shouldPlay = false;
			// 	//
			// 	// }
			//
			// }
		}

		void OnTriggerEnter(Collider other){
		//if(!alreadyPlayed){
				if(distortSound != null){
					if(other.tag == "Player" )
					{
						inTrigger = true;
				}
		 }
		}

		void OnTriggerExit(Collider other){
		//if(!alreadyPlayed){
				if(distortSound != null){
					if(other.tag == "Player" )
					{
						inTrigger = false;
					}
			 }
		}
	}
}
