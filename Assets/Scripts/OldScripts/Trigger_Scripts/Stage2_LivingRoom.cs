﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace S3{
public class Stage2_LivingRoom : MonoBehaviour {

		private AudioSource livingRoomAudio;
		public AudioClip clip;
		public AudioClip clip2;
		public GameObject wall1;
		public GameObject wall2;
		private Collider c1;
		private Collider c2;
		private bool isDone = false;


		void SetInitialReferences()
		{
			livingRoomAudio = this.GetComponent<AudioSource> ();
			livingRoomAudio.clip = clip;
			c1 = wall1.GetComponent<Collider> ();
			c2 = wall2.GetComponent<Collider> ();
		}

		void OnEnable()
		{
			SetInitialReferences ();
			c1.isTrigger = false;
			c2.isTrigger = false;
		}

		void OnTriggerEnter(Collider other)
		{
			if (other.tag == "Player") {
				if (!isDone) {
					StartCoroutine (waitTime ());
				}
			}
		}
			
		IEnumerator waitTime()
		{
			
			livingRoomAudio.Play ();
			yield return new WaitForSeconds(8);
			livingRoomAudio.clip = clip2;
			livingRoomAudio.Play ();
			yield return new WaitForSeconds(2);
			c1.isTrigger = true;
			c2.isTrigger = true;
			isDone = true;

		}

	}
}
