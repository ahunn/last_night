﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace S3
{
	public class Stage1 : MonoBehaviour {

		//Hallway
		public GameObject daughter;
		public GameObject daughterAttic;
		public GameObject door;
		public GameObject TeleportDoor;
		public GameObject trigger;
		public Collider blockStairs;

		//Attic 
		public GameObject doorLockTrigger;
		private Collider lockInit;
		public GameObject crib;
		private Collider cribCollider;
		public GameObject NightmarePrefab;
		private Stage1_AtticDoor lockAtticDoor;
		private Stage1_Crib activateCrib;

		//Audio
		private AudioSource dialogue;
		private AudioSource doorNoise;
		public AudioClip daughterSpeak;
		public AudioClip scream;
		public AudioClip slam;
		private Renderer b;
		bool lightFlick = false;

		//Lights
		private Light_Controller lights;

		private bool stop = false;

		//Enable the collider to block exit, audio tweaking, and 
		void OnEnable(){
			SetInitialReferences ();
			//if (!stop) {
				//hallwayScene ();
				//stop = true;
			//}
			blockStairs.enabled = true;
		}

		//Grab and set first round of audio sources and colliders. 
		void SetInitialReferences(){
			dialogue = daughter.GetComponent<AudioSource> ();
			doorNoise = door.GetComponent<AudioSource> ();
			cribCollider = crib.GetComponent<Collider> ();
			lights = this.GetComponent<Light_Controller> ();
			lockAtticDoor = doorLockTrigger.GetComponent<Stage1_AtticDoor> ();
		}

		/**
		 * Hallway Scene consists of:
		 * ***Once opening scene and level loads, dialogue starts 	DONE
		 * ***Lights then flicker									DONE
		 * ***Then she gets grabbed									DONE
		 * ***Attic door slams shut									DONE
		 * ***Once player walks through, locks behind them		
		 * ***Daughter spawns in front of crib
		 * ***Dialogue
		 * ***Once triggered, blood appears, lights flicker, daughter replaced with Nightmare
		 * ***Player control taken away, nightmare fills the screen
		 * ***Lights flicker and disapears, door appears
		 * ***Once player goes through door, increments scene. Resets Scene1 and advances to Scene2
		 * */
		public void hallwayScene()
		{
			dialogue.clip = daughterSpeak;

			StartCoroutine(DialogToGrab());


	
		}

		void Update()
		{
			if (lightFlick)
				lights.FlickerLights ();
			atticScene ();
		}
		IEnumerator DialogToGrab(){
			bool done = false;
			dialogue.Play();
			yield return new WaitForSeconds(15);
			lightFlick = true;
			yield return new WaitForSeconds(2);
			StartCoroutine (Move (daughter, door, 5f));
			door.GetComponent<Animator>().Play ("Door_Close");
			door.GetComponent<Animator> ().ResetTrigger ("open");
			door.GetComponent<Animator> ().speed = 10f;
			yield return new WaitForSeconds(5);
			lightFlick = false;
			lights.NormalLights ();
			door.GetComponent<Animator> ().speed = 1f;
		} 

		IEnumerator Move(GameObject x, GameObject y, float speed){
			dialogue.clip = scream;
			dialogue.Play();
			while (x.transform.position != y.transform.position) {
				x.transform.position =
					Vector3.MoveTowards (x.transform.position, y.transform.position, speed * Time.deltaTime);
				yield return new WaitForEndOfFrame ();
			}
		}




		void atticScene()
		{
			if (lockAtticDoor.entered ()) {
				door.GetComponent<Animator> ().ResetTrigger ("open");
				door.GetComponent<Animator> ().enabled = false;
			}

			if (activateCrib.entered ()) {
				lightFlick = true;
				daughterAttic.GetComponent<Renderer> ().enabled = false;
				NightmarePrefab.GetComponent<Renderer>().enabled = true;
				TeleportDoor.GetComponent<Renderer>().enabled = true;
			}
		}
	}
}
