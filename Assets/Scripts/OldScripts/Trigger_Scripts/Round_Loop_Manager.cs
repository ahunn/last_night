﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace S3
{
	public class Round_Loop_Manager : MonoBehaviour {

		//Destination
		public GameObject otherPortal;
		private Trigger_Master triggerMaster;
		private Collider myCollider;


		public GameObject playerTrigger;
		private Spawn_Controller sp;

		//Level counter, incremented by passes through door
		public int currentLevel = 0;
		private int level;

		private int rand;
		public AudioSource voice;
		public float defaultVolume = 0;
		private Stage1 s1;



		void Update()
		{
			//if (currentLevel == level)
				//forceUpdate ();
		}
			

		//Stage 1 - Hallway:

		//Game Start
		//Door default state is open
		//Daughter begins dialogue
		//Towards middle, begin light flicker
		//Start translation to the left, despawn daughter
		//Slam attic door and play noise
	//Stage 1- Attic:
		//Music changes
		//Daughter spawns in front of crib
		//Lights flickers when in trigger zone
		//Enemy spawns in her place
		//On enemy death, door spawns behind the player
		//Door to attic locked
	//Stage 2- Bathroom(Optional):
		//Door opens
		//Door closes behind them
		//Audio plays
		//Blood texture maps are placed
		//Door opens when audio finishes
		//Enemy spawns(percentage of higher level enemy)
	//Stage 2- Living Room
		//Control taken away from player
		//Audio plays
		//Enemies appear
		//Door apears after their death
		//Screams heard from door
		//Area blocked off
	//Stage 3- Daughter's Room
		//Voice directs them to find her 
		//Teddy bears follow player
		//Teddy bears turn to enemies
		//Last one is daughter, dead
		//Lights flicker when this happens
		//Exit turns into entrance to basement
	//Stage 3-Basement
		//Timed decision, kill or not kill
		//If killed, negative audio plays, stronger enemy appears
		//Else, positive audio plays, multiple weak enemies appear
		//House unlocks completely
		//Audio says find the door
	//Stage 4- The Door:
		//Once found, transport to new scene


		void Start ()
		{
			SetInitialReferences();
			SetTriggerEvents();
			s1.hallwayScene ();

		}

		void SetInitialReferences()
		{
			level = currentLevel;
			s1 = this.GetComponent<Stage1> ();
			s1.hallwayScene ();

			myCollider = this.GetComponent<Collider>();
			sp = playerTrigger.GetComponent<Spawn_Controller>();
			//RadioTrig = GameObject.FindGameObjectWithTag("Radio");

		}
		void OnEnable()
		{
			SetInitialReferences();
			SetTriggerEvents();
		}

		void SetTriggerEvents()
		{
			rand = Random.Range(0,5);
			//Trigger_OnRadioEnter rad = RadioTrig.GetComponent<Trigger_OnRadioEnter>();
			//rad.isActive(currentLevel);
			Debug.Log("Radio");

		}

		void OnTriggerEnter(Collider other){
			if(other.tag == GameManager_References._playerTag){
				other.transform.position = otherPortal.transform.position + otherPortal.transform.forward * 1;
				currentLevel ++;
				SetTriggerEvents();
				StartCoroutine(waitTime());
			}
		}

		IEnumerator waitTime()
		{
			if(currentLevel == rand || (currentLevel + 1) == rand)
			{
				Debug.Log("enemy behind you");
				//AudioSource.PlayClipAtPoint(voice,transform.position,defaultVolume);
				yield return new WaitForSeconds(8);
				//sp.spawnSequence();

			}
		}
	}
}
