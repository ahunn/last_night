﻿using UnityEngine;
using System.Collections;
namespace S3
{
	public class Trigger_OnMainDoor : MonoBehaviour {
		private Door doorScript;
	//	private Trigger_Master triggerMaster;


		void OnEnable()
		{
			SetInitialReferences();
		//	triggerMaster.EventEnterMainDoorTrigger += OnTriggerEnter;
		//	triggerMaster.EventEnterMainDoorTrigger += OnTriggerExit;
		}

		void OnDisable()
		{
			//triggerMaster.EventEnterMainDoorTrigger -= OnTriggerEnter;
			//triggerMaster.EventEnterMainDoorTrigger -= OnTriggerExit;
		}

		void SetInitialReferences()
		{
			doorScript = GetComponent<Door>();
			//triggerMaster = GetComponent<Trigger_Master>();
		}

		void OnTriggerEnter(Collider other)
		{

			if(other.tag == "Player")
			{
				doorScript.ChangeDoorState();
			}


		}

		void OnTriggerExit(Collider other)
		{
			if(other.tag == "Player")
			{
				StartCoroutine(DisableDoor());

			}
		}

		IEnumerator DisableDoor()
			{
				doorScript.ChangeDoorState();
				yield return new WaitForSeconds(1);
				doorScript.enabled = false;
				this.enabled = false;
			}
	}



}
