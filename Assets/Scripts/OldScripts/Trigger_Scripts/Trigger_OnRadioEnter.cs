using UnityEngine;
using System.Collections;

namespace S3
{

	public class Trigger_OnRadioEnter: MonoBehaviour
	{
		//private Trigger_Master triggerMaster;
		public float defaultVolume = 5;
		public float defVol = 1;

		public AudioClip radioSound;
	 	public AudioClip distortSound;
		public string Interact;
		public AudioClip dontTouch;

		private bool alreadyPlayed = false;
		private GameObject temp;
		private GameObject spawnRadio;
		// 	private Collider myCollider;
		private bool shouldPlay = false;
		private bool inTrigger = false;
		private bool distortPlay = false;
		private Collider myCollider;

		void OnEnable()
		{
			SetInitialReferences();
			PlaySounds();	//Default Sound No Trigger
		}
		void Update()
		{
			if(inTrigger)
			{
				if(Input.GetButtonDown(Interact)){

						AudioSource.PlayClipAtPoint(dontTouch,(transform.position),defaultVolume);
			}
			}
		}
		void SetInitialReferences()
		{
			//triggerMaster = GetComponent<Trigger_Master>();

			if(GetComponent<Collider>() != null)
			{
				myCollider = GetComponent<Collider>();
			}

		//myCollider.enabled = false;
			spawnRadio =GameObject.FindGameObjectWithTag("SpawnRadio");
		}
		void PlaySounds()
		{
				if(radioSound != null)
				{
					defaultVolume = 5;
					AudioSource.PlayClipAtPoint(radioSound,(transform.position),defaultVolume);
				}
			}

			void PlaySounds2()
			{
					if(!shouldPlay)
					{
						defaultVolume = 5;
						AudioSource.PlayClipAtPoint(distortSound,(transform.position),defaultVolume);
						shouldPlay = true;
					}
				}

		public void isActive(int level)
		{
				if(level == 2)
				{
					 temp = GameObject.Find("One shot audio");
					 Destroy(temp);
				}
				if(level == 3)
				{
					PlaySounds2();
					//myCollider.enabled = true;
				}
			}

			void OnTriggerEnter(Collider other){
			//if(!alreadyPlayed){
					if(distortSound != null){
						if(other.tag == "Player" )
						{
							inTrigger = true;
						}
				 }
			}

			void OnTriggerExit(Collider other){
			//if(!alreadyPlayed){
					if(distortSound != null){
						if(other.tag == "Player" )
						{
							inTrigger = false;
						}
				 }

			}

		// IEnumerator DelaySpawn()
		// 	{
		// 		yield return new WaitForSeconds(15);
		// 		spawnRadio.GetComponent<SpawnerProximity>().enabled = true;
		// 		AudioSource.PlayClipAtPoint(numberSound,(transform.position),defaultVolume);
		// 	}
	}
}
