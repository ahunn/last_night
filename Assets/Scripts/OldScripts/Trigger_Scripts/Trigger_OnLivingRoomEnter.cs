using UnityEngine;
using System.Collections;

namespace S3
{

	public class Trigger_OnLivingRoomEnter: MonoBehaviour
	{
		private Renderer DisapearingWall;
		public float defaultVolume = 0;
		private Collider WallCollider;
		private bool shouldPlay = false;
		private bool alreadyPlayed = false;
		public AudioClip livingRoomTrigger;

		void OnEnable()
		{
			SetInitialReferences();
			//PlaySounds();
		}
		void Update()
		{
		//DisapearingWallTrigger();
		}
		void SetInitialReferences()
		{
			DisapearingWall = GetComponent<Renderer>();
			WallCollider = GetComponent<Collider>();
		}

		// void DisapearingWallTrigger()
		// {
		// 	if(shouldPlay)
		// 	{
		// 		if(DisapearingWall != null)
		// 		{
		// 			DisapearingWall.enabled = false;
		// 		}
		// 		// if(WallCollider != null)
		// 		// {
		// 		// 	WallCollider.enabled = false;
		// 		// }
		// 	}
		// }

		public void isActive(int level)
		{
			if(level < 5)
			{
				defaultVolume = 0;
				shouldPlay = false;
				this.enabled = false;
			}
			else{
				shouldPlay = true;
				defaultVolume = 3;
				this.enabled = true;
				WallCollider.isTrigger = true;
				DisapearingWall.enabled = false;
			}
		}

		void OnTriggerEnter(Collider other){
			if(!alreadyPlayed)
			{
				if(shouldPlay)
				{
					if(livingRoomTrigger != null)
					{
						if(other.tag == "Player")
						{
							AudioSource.PlayClipAtPoint(livingRoomTrigger,transform.position,defaultVolume);
							alreadyPlayed = true;
						}
					}
				}
			}
		}
	}
}
