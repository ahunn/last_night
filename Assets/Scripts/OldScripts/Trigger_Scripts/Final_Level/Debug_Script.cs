﻿using UnityEngine;
using System.Collections;

namespace S3
{
	public class Debug_Script : MonoBehaviour
	{

		private Light_Controller lights;


		void SetInitialReferences()
		{
			lights = GetComponent<Light_Controller>();

		}

		void OnEnable()
		{
			SetInitialReferences();
		}

		void OnTriggerEnter(Collider other)
		{
			if(other.tag == "Player")
			{
				//lights.NormalLights();
				//lights.DimmedRange();
				//lights.DimLights
				lights.ChangeToRed();
			}
		}

		void OnTriggerExit(Collider other)
		{
			if(other.tag == "Player")
			{
				//lights.NormalLights();
				//lights.DimmedRange();
				//lights.DimLights
				//lights.test();
			}
		}

	}

}
