using UnityEngine;
using System.Collections;

namespace S3
{

	public class Trigger_FlickerScareEnd: MonoBehaviour
	{
		public Light firstLight;
		public Light secondLight;
		public Light thirdLight;
		public Light fourthLight;
		public Light fifthLight;
		public Light flashlight;

		public bool continueLoop = true;		//Locked door until event triggered
		private bool hasTriggerEnded = false;	//Makes sure event is a one time occurance

		public Collider triggerCollider;
		//private Collider otheCollider;
		private GameObject objectToSpawn;
		private GameObject rlm; 			//Round Loop Object
		public AudioClip disapear;		//On End, audio que plays
		public float defaultVolume = 0;

		private Trigger_FlickerScareStart tfSS;	//Need reference to first part of event

		void OnEnable()
		{
			SetInitialReferences();
		}
		void Update()
		{
			checkState();
		}
		void SetInitialReferences()
		{
			tfSS = GameObject.FindGameObjectWithTag("Trigger1").GetComponent<Trigger_FlickerScareStart>();
			rlm = GameObject.Find("LoopStart");
		}
		void checkState()
		{
			//If first event triggered already, activate second event
			if(tfSS.stateCheck() == false){
				triggerCollider.enabled = true;
			}
		}
		//If Round is on 2, activate this trigger
		public void isActive(int level)
		{
			//Trigger must be completed before you can continue
			if(level == 3)
			{
				continueLoop = false;
			}
			if(level < 3)
			{
				triggerCollider.enabled = false;
			}
			else
			{
				checkState();
			}
		}

		//If it hasnt played already and tag matches player...
		void OnTriggerEnter(Collider other)
		{
			if(!hasTriggerEnded)
			{
				if(other.name == "Player")
				{
					StartCoroutine(DelayLights());
				}
			}
		}
		//Finds Cloned Boss, sets continue loop to true, plays audio, disables the first
		//half of the trigger, disables lights in the surrounding area, then deactivates cloned Boss
		//Lights turn back on, trigger deactivates
		IEnumerator DelayLights()
		{
				objectToSpawn = GameObject.FindGameObjectWithTag("CloneBoss");
				continueLoop = true;
				AudioSource.PlayClipAtPoint(disapear,transform.position,defaultVolume);
				tfSS.hasTriggerStarted = false;
				hasTriggerEnded = true;
				firstLight.enabled = false;
				secondLight.enabled = false;
				thirdLight.enabled = false;
				fourthLight.enabled = false;
				fifthLight.enabled = false;
				flashlight.enabled = false;
				yield return new WaitForSeconds(2);
				objectToSpawn.SetActive(false);
				firstLight.enabled = true;
				secondLight.enabled = true;
				thirdLight.enabled = true;
				fourthLight.enabled = true;
				fifthLight.enabled = true;
				flashlight.enabled = true;
				GetComponent<Collider>().enabled = false;
				rlm.GetComponent<Collider>().enabled = true;
		}
	}
}
