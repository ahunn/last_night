using UnityEngine;
using System.Collections;
namespace S3
{
	public class Trigger_OnFrontDoorEnter: MonoBehaviour {

		private bool alreadyPlayed = false;
		private Collider myCollider;
		public AudioClip doorTrigger;
		public float defaultVolume = 2;
		private bool shouldPlay = false;

		void Start()
		{
			SetInitialReferences();
			myCollider.enabled = false;
		}

		void SetInitialReferences()
		{
		 myCollider = GetComponent<Collider>();
		}

		void OnTriggerEnter(Collider other)
		{
			if(shouldPlay)
			{
				//doorScript.enabled = true;
				if(other.tag == "Player")
				{
					AudioSource.PlayClipAtPoint(doorTrigger,transform.position,defaultVolume);
				}
			}
			// else{
			// 	doorScript.enabled = false;
			// }
		}


		public void isActive(int level)
		{
			if(level < 0)
			{
				shouldPlay = false;
				myCollider.enabled = false;
				//doorScript.enabled = false;
				//this.enabled = false;
			}else{
				shouldPlay = true;
				myCollider.enabled = true;
				//doorScript.enabled = true;
			//	this.enabled = true;
			}
		}


		// IEnumerator DelayDoor()
		// 	{
		// 		Debug.Log("EnterDoor");
		// 		yield return new WaitForSeconds(15);
		// 		Debug.Log("TimmerStoped");
		// 		doorScript.ChangeDoorState();
		//
		// 		//Debug.Log("Locked");
		// 		//doorScript.enabled = false;
		// 		//this.enabled = false;
		// 	}
	}
}
