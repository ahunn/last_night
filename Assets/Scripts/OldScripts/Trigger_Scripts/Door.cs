using UnityEngine;
using System.Collections;

namespace S3
{
	public class Door : MonoBehaviour {

		public bool open = false;
		public Vector3 OpenPosition;
		public float oAngle = 90f;
		public float cAngle = 0f;
		public float smooth = 2f;


		public void ChangeDoorState()
		{
			open = !open;
		}

		// Update is called once per frame
		void Update ()
		{
			if (open) { //this means open == true
				Quaternion targetRotation = Quaternion.Euler(0,oAngle, 0);
				transform.localRotation = Quaternion.Slerp (transform.localRotation, targetRotation, smooth * Time.deltaTime);
			}
			else
			{
				Quaternion targetRotation2 = Quaternion.Euler(0,cAngle, 0);
				transform.localRotation = Quaternion.Slerp (transform.localRotation, targetRotation2, smooth * Time.deltaTime);
			}

		}
	}
}
