using UnityEngine;
using System.Collections;

namespace S3
{
	public class Trigger_FlickerScareStart: MonoBehaviour
	{
		public Light firstLight;
		public Light secondLight;
		public Light thirdLight;
		public Light fourthLight;
		public Light fifthLight;
		public Light flashlight;

		public bool isEnabled = true;
		public GameObject objectToSpawn;
		private Collider triggerCollider;
		public bool hasTriggerStarted = false;
	//	public GameObject ScareEnemy;
		private bool stopThis = false;
		public AudioClip voice;
		public float defaultVolume = 0;

		void OnEnable()
		{
			SetInitialReferences();
		}
		void SetInitialReferences()
		{
			triggerCollider = GetComponent<Collider>();
		}
		//If trigger started, flicker lights unti trigger is deactivated
		void Update()
		{
			if(hasTriggerStarted)
			{
				FlickerLights(firstLight);
				FlickerLights(secondLight);
				FlickerLights(thirdLight);
				FlickerLights(fourthLight);
				FlickerLights(fifthLight);
			}
		}

		//Activates trigger on round 2
		public void isActive(int level)
		{
			if(level < 3)
			{
				triggerCollider.enabled = false;
			}
			else
			{
				if(!stopThis)
				{
					triggerCollider.enabled = true;
				}
			}
		}
		//Used for second half of script
		public bool stateCheck()
		{
			return isEnabled;
		}

		//Using a random number to flicker the lights
		void FlickerLights(Light i)
		{
			float RandomNumber = Random.value;
			if(RandomNumber <=.2)
			{
				i.enabled = true;
			}
			else
			{
				i.enabled = false;
			}
		}

		void OnTriggerEnter(Collider other)
		{
			if(!hasTriggerStarted)
			{
				if(other.name == "Player")
				{
					StartCoroutine(DelayLights());
				}
			}
		}

		IEnumerator DelayLights()
		{
			AudioSource.PlayClipAtPoint(voice,transform.position,defaultVolume);
			hasTriggerStarted = true;
			FlickerLights(flashlight);
			FlickerLights(flashlight);
			FlickerLights(flashlight);
			flashlight.enabled = false;
			yield return new WaitForSeconds(8);
			firstLight.enabled = false;
			secondLight.enabled = false;
			Instantiate(objectToSpawn,new Vector3(-20.77f, 1.43f, -9.36f),transform.rotation);
			hasTriggerStarted = true;
			isEnabled = false;
			triggerCollider.enabled = false;
			yield return new WaitForSeconds(5);
			hasTriggerStarted = false;
			flashlight.enabled = true;
			stopThis = true;
			this.enabled = false;
		}
	}
}
