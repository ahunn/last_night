using UnityEngine;
using System.Collections;

namespace S3
{

	public class Trigger_OnLivingRoomSpawn: MonoBehaviour
	{

		private SpawnerProximity spawn;
		private bool shouldPlay = false;
		private bool hasPlayed = false;

		void Start()
		{
			SetInitialReferences();
		}
		void SetInitialReferences()
		{
				spawn = GetComponent<SpawnerProximity>();
		}

		public void isActive(int level)
		{
			if(level < 5)
			{
				shouldPlay = false;
				this.enabled = false;
			}
			else{
				shouldPlay = true;
				this.enabled = true;
			}
		}

		void OnTriggerEnter(Collider other){
				if(shouldPlay == true){
					if(hasPlayed == false)
					{
						if(other.tag == "Player")
						{
								spawn.enabled = true;
								hasPlayed = true;
						}
					}
			}
		}
	}
}
