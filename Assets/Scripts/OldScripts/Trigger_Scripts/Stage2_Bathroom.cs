﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace S3{
	public class Stage2_Bathroom : MonoBehaviour {

		private AudioSource bathroomAudio;
		public AudioClip clip;
		public GameObject door;
		private bool finished = false;

		//private BloodTrail trail;
		public Renderer a;
		public Renderer b;
		public Renderer c;
		public Renderer d;
		public Renderer e;

		void SetInitialReferences()
		{
			bathroomAudio = this.GetComponent < AudioSource> ();
			a.enabled = false;
			b.enabled = false;
			c.enabled = false;
			d.enabled = false;
			e.enabled = false;
		}

		void ToggleTrail()
		{
			a.enabled = !a.enabled;
			b.enabled = !b.enabled;
			c.enabled = !c.enabled;
			d.enabled = !d.enabled;
			e.enabled = !e.enabled;
		}
			
		void OnTriggerEnter(Collider other)
		{
			if (!finished)
				StartCoroutine (waitTime ());
		}
	
		IEnumerator waitTime()
		{
			door.GetComponent<Animator>().Play ("Door_Close");
			door.GetComponent<Animator> ().enabled = false;
			bathroomAudio.clip = clip;
			bathroomAudio.Play ();
			ToggleTrail();	
			yield return new WaitForSeconds(8);
			door.GetComponent<Animator> ().enabled = true;
			door.GetComponent<Animator>().Play ("Door_open");
			finished = true;
		}
	}
}
