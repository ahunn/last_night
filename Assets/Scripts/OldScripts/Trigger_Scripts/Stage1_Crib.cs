﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace S3{
	public class Stage1_Crib: MonoBehaviour {
		bool isEntered = false;
		void OnTriggerEnter(Collider other){
			if(other.tag == GameManager_References._playerTag){
				isEntered = true;
			}
		}

		public bool entered()
		{
			return isEntered;
		}
	}
}