﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace S3
{
	public class LoopingPortal : MonoBehaviour {

		//Destination
		public GameObject otherPortal;
		private Trigger_Master triggerMaster;

		private GameObject RadioTrig;
		private GameObject BathroomTrig;
		private GameObject TelephoneTrig;
		private GameObject FrontDoorTrig;
		private GameObject BedroomTrig;
		private GameObject LivingRoomTrig;

		public int currentLevel = 0;


		void Start ()
		{
			SetInitialReferences();



			SetTriggerEvents();
		}

		void SetInitialReferences()
		{
			triggerMaster = GetComponent<Trigger_Master>();


			BathroomTrig = GameObject.FindGameObjectWithTag("Bathroom");
			TelephoneTrig = GameObject.FindGameObjectWithTag("Telephone");
			FrontDoorTrig = GameObject.FindGameObjectWithTag("FrontDoor");
			BedroomTrig	= GameObject.FindGameObjectWithTag("Bedroom");
			LivingRoomTrig	=GameObject.FindGameObjectWithTag("LivingRoom");
		}
		void OnEnable()
		{
			SetInitialReferences();



			SetTriggerEvents();
		}

		void OnDisable()
		{

		}

		void SetTriggerEvents()
		{
RadioTrig = GameObject.FindGameObjectWithTag("Radio");
			Trigger_OnRadioEnter rad = RadioTrig.GetComponent<Trigger_OnRadioEnter>();
			rad.isActive(currentLevel);

			// Trigger_OnBathroomEnter tub = BathroomTrig.GetComponent<Trigger_OnBathroomEnter>();
			// tub.isActive(currentLevel);
			//
			// Trigger_OnTelephoneEnter pho = TelephoneTrig.GetComponent<Trigger_OnTelephoneEnter>();
			// pho.isActive(currentLevel);
			//
			// Trigger_OnFrontDoorEnter fro = FrontDoorTrig.GetComponent<Trigger_OnFrontDoorEnter>();
			// fro.isActive(currentLevel);
			//
			// Trigger_OnBedroomEnter bed = BedroomTrig.GetComponent<Trigger_OnBedroomEnter>();
			// bed.isActive(currentLevel);
			//
			// Trigger_OnLivingRoomEnter liv = LivingRoomTrig.GetComponent<Trigger_OnLivingRoomEnter>();
			// liv.isActive(currentLevel);

		}

		void OnTriggerEnter(Collider other){
			if(other.tag == GameManager_References._playerTag){
				other.transform.position = otherPortal.transform.position + otherPortal.transform.forward * 1;
				currentLevel ++;
				SetTriggerEvents();
			}
		}
	}
}
