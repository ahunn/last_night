﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace S3{
	public class Survival_Master : MonoBehaviour {

		public GameObject player;

		private Time_Count time;

		public GameObject livingRoomSpawn;
		public GameObject sideDoorSpawn;
		public GameObject bathroomSpawn;
		public GameObject officeSpawn;
		public GameObject kitchenSpawn;
		public GameObject mainRoomSpawn;
		public GameObject daughterSpawn;
		public GameObject atticSpawn;
		public GameObject secondFloorSpawn;
		private AudioSource myAudioSource;
		public AudioClip startClip;
		public AudioClip tormentClip;
		public AudioClip newsClip;
		public AudioClip daughterClip;
		public AudioClip remarkClip;
		public AudioClip endClip;
		public AudioClip startingSound;
		public AudioClip startRoundSound;
		public AudioClip endOfRoundClip;
		public AudioClip winnerAudio;

		public GameObject nightmarePrefab;
		private Spawn_Controller livingRoom;
		private Spawn_Controller sideDoor;
		private Spawn_Controller bathroom;
		private Spawn_Controller office;
		private Spawn_Controller kitchen;
		private Spawn_Controller mainRoom;
		private Spawn_Controller daughter;
		private Spawn_Controller attic;
		private Spawn_Controller secondFloor;

		public GameObject ammo;
		public GameObject health;

		private Light_Controller lights;
		public Text levelText;   
		private float level;
		public Text gameOverText;


		private GameManager_GameOver gameOverScreen;

		void SetInitialReferences(){
			level = 1;
			myAudioSource = this.GetComponent<AudioSource>();
			livingRoom = livingRoomSpawn.GetComponent<Spawn_Controller>();
			sideDoor = sideDoorSpawn.GetComponent<Spawn_Controller>();
			bathroom= bathroomSpawn.GetComponent<Spawn_Controller>();
			office= officeSpawn.GetComponent<Spawn_Controller>();
			kitchen= kitchenSpawn.GetComponent<Spawn_Controller>();
			mainRoom= mainRoomSpawn.GetComponent<Spawn_Controller>();
			daughter= daughterSpawn.GetComponent<Spawn_Controller>();
			attic= atticSpawn.GetComponent<Spawn_Controller>();
			secondFloor= secondFloorSpawn.GetComponent<Spawn_Controller>();
			lights = this.GetComponent<Light_Controller> ();
			lights.ToggleFirstFloor (false);
			lights.ToggleSecondFloor (true);
			gameOverScreen = this.GetComponent<GameManager_GameOver> ();


		}

		void Start(){
			SetInitialReferences ();
			//StartCoroutine (DropRefresh ());
			StartCoroutine(SpawnTime (11));
		}

		void Update()
		{
			levelText.text = level.ToString();
		}

		void Winner()
		{

		}


		IEnumerator SpawnTime(int levels){

			 int spawnCount = 1;
			myAudioSource.clip = startingSound;
			myAudioSource.Play ();
			yield return new WaitForSeconds (startingSound.length);



			for (int i = 0; i < levels; i++) {
				if (i == 0) {
					livingRoomSpawn.SetActive (true);
					sideDoorSpawn.SetActive (true);
					kitchenSpawn.SetActive (true);
					secondFloorSpawn.SetActive (true);
					mainRoomSpawn.SetActive (false);
					daughterSpawn.SetActive (false); 

					myAudioSource.clip = startClip;
					myAudioSource.Play ();
					yield return new WaitForSeconds ((myAudioSource.clip).length);
					livingRoom.spawnEnemy (health, livingRoomSpawn, 1);	//PREFAB AND NUMBER PARAMS
					sideDoor.spawnEnemy (ammo, sideDoorSpawn, 1);	//PREFAB AND NUMBER PARAMS
					kitchen.spawnEnemy (nightmarePrefab, kitchenSpawn, spawnCount);	//PREFAB AND NUMBER PARAMS
					secondFloor.spawnEnemy (nightmarePrefab, secondFloorSpawn, spawnCount);	//PREFAB AND NUMBER PARAMS


				} else if (i == 1) {
					livingRoomSpawn.SetActive (true);
					sideDoorSpawn.SetActive (true);
					kitchenSpawn.SetActive (true);
					secondFloorSpawn.SetActive (true);
					mainRoomSpawn.SetActive (false);
					daughterSpawn.SetActive (false); 

					myAudioSource.clip = remarkClip;
					myAudioSource.Play ();
					yield return new WaitForSeconds ((myAudioSource.clip).length);
					kitchen.spawnEnemy (health, kitchenSpawn, 1);	//PREFAB AND NUMBER PARAMS
					secondFloor.spawnEnemy (ammo, secondFloorSpawn, 1);	//PREFAB AND NUMBER PARAMS
					livingRoom.spawnEnemy (nightmarePrefab, livingRoomSpawn, spawnCount);	//PREFAB AND NUMBER PARAMS
					sideDoor.spawnEnemy (nightmarePrefab, sideDoorSpawn, spawnCount);	//PREFAB AND NUMBER PARAMS
				} else if (i == 3) {
					livingRoomSpawn.SetActive (false);
					sideDoorSpawn.SetActive (true);
					kitchenSpawn.SetActive (true);
					secondFloorSpawn.SetActive (false);
					mainRoomSpawn.SetActive (true);
					daughterSpawn.SetActive (true); 

					spawnCount = 2;
					myAudioSource.clip = tormentClip;
					myAudioSource.Play ();
					yield return new WaitForSeconds ((myAudioSource.clip).length);
					mainRoom.spawnEnemy (health, mainRoomSpawn, 1);	//PREFAB AND NUMBER PARAMS
					daughter.spawnEnemy (ammo, daughterSpawn, 1);	//PREFAB AND NUMBER PARAMS
					sideDoor.spawnEnemy (nightmarePrefab, sideDoorSpawn, spawnCount);	//PREFAB AND NUMBER PARAMS
					kitchen.spawnEnemy (nightmarePrefab, kitchenSpawn, spawnCount);	//PREFAB AND NUMBER PARAMS

				} else if (i == 5) {
					livingRoomSpawn.SetActive (true);
					sideDoorSpawn.SetActive (false);
					kitchenSpawn.SetActive (false);
					secondFloorSpawn.SetActive (true);
					mainRoomSpawn.SetActive (true);
					daughterSpawn.SetActive (true); 

					spawnCount = 3;
					myAudioSource.clip = daughterClip;
					myAudioSource.Play ();
					yield return new WaitForSeconds ((myAudioSource.clip).length);
					livingRoom.spawnEnemy (health, livingRoomSpawn, 1);	//PREFAB AND NUMBER PARAMS
					secondFloor.spawnEnemy (ammo, secondFloorSpawn, 1);	//PREFAB AND NUMBER PARAMS
					mainRoom.spawnEnemy (nightmarePrefab, mainRoomSpawn, spawnCount);	//PREFAB AND NUMBER PARAMS
					daughter.spawnEnemy (nightmarePrefab, daughterSpawn, spawnCount);	//PREFAB  NUMBER PARAMS
				} else if (i == 7) {

					livingRoomSpawn.SetActive (true);
					sideDoorSpawn.SetActive (true);
					kitchenSpawn.SetActive (true);
					secondFloorSpawn.SetActive (false);
					mainRoomSpawn.SetActive (true);
					daughterSpawn.SetActive (false); 

					spawnCount = 4;
					myAudioSource.clip = endClip;
					myAudioSource.Play ();
					yield return new WaitForSeconds ((myAudioSource.clip).length);
					sideDoor.spawnEnemy (ammo, sideDoorSpawn, 1);	//PREFAB AND NUMBER PARAMS
					kitchen.spawnEnemy (health, kitchenSpawn, 1);	//PREFAB AND NUMBER PARAMS
					mainRoom.spawnEnemy (nightmarePrefab, mainRoomSpawn, spawnCount);	//PREFAB AND NUMBER PARAMS
					livingRoom.spawnEnemy (nightmarePrefab, livingRoomSpawn, spawnCount);	//PREFAB AND NUMBER PARAMS
				} else if (i == 9) {
					livingRoomSpawn.SetActive (true);
					sideDoorSpawn.SetActive (true);
					kitchenSpawn.SetActive (true);
					secondFloorSpawn.SetActive (true);
					mainRoomSpawn.SetActive (true);
					daughterSpawn.SetActive (true); 

					myAudioSource.clip = endClip;
					myAudioSource.Play ();
					yield return new WaitForSeconds ((myAudioSource.clip).length);
					spawnCount = 4;
					livingRoom.spawnEnemy (nightmarePrefab, livingRoomSpawn, spawnCount);	//PREFAB AND NUMBER PARAMS
					sideDoor.spawnEnemy (nightmarePrefab, sideDoorSpawn, spawnCount);	//PREFAB AND NUMBER PARAMS
					kitchen.spawnEnemy (nightmarePrefab, kitchenSpawn, spawnCount);	//PREFAB AND NUMBER PARAMS
					secondFloor.spawnEnemy (nightmarePrefab, secondFloorSpawn, spawnCount);	//PREFAB AND NUMBER PARAMS
					mainRoom.spawnEnemy (nightmarePrefab, mainRoomSpawn, spawnCount);	//PREFAB AND NUMBER PARAMS
					daughter.spawnEnemy (nightmarePrefab, daughterSpawn, spawnCount);	//PREFAB AND NUMBER PARAMS
				} else if (i == 11) {
					livingRoomSpawn.SetActive (false);
					sideDoorSpawn.SetActive (false);
					kitchenSpawn.SetActive (false);
					secondFloorSpawn.SetActive (false);
					mainRoomSpawn.SetActive (false);
					daughterSpawn.SetActive (false); 
					gameOverScreen.TurnOnGameOverPanel ();
					gameOverText.text = ("Winner");
					myAudioSource.clip = winnerAudio;
					myAudioSource.Play ();

				}else {
					livingRoomSpawn.SetActive (true);
					sideDoorSpawn.SetActive (false);
					kitchenSpawn.SetActive (true);
					secondFloorSpawn.SetActive (true);
					mainRoomSpawn.SetActive (false);
					daughterSpawn.SetActive (false); 
					myAudioSource.clip = startRoundSound;
					myAudioSource.Play ();
					yield return new WaitForSeconds ((myAudioSource.clip).length);
					livingRoom.spawnEnemy (health, livingRoomSpawn, 1);	//PREFAB AND NUMBER PARAMS
					kitchen.spawnEnemy (nightmarePrefab, kitchenSpawn, spawnCount);	//PREFAB AND NUMBER PARAMS
					secondFloor.spawnEnemy (nightmarePrefab, secondFloorSpawn, spawnCount);	//PREFAB AND NUMBER PARAMS
				}
	
				//Once player kills all nightmare clones, play audio and unlock living room
				while (GameObject.Find("Nightmare(Clone)") == true)
					yield return null;


				level++;
			}
			myAudioSource.clip = endOfRoundClip;
			myAudioSource.Play ();
		}
	}
}
