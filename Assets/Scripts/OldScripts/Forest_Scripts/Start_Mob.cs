﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
namespace S3
{
	public class Start_Mob : MonoBehaviour {
		//Disables floor and plays audio cue
		public Collider myCollider;
		public Collider myCollider2;
		public AudioClip voice;

		void OnTriggerEnter(Collider other)
		{
			if(other.tag == "Player")
			{
				myCollider.enabled = false;
				myCollider2.enabled = false;
				AudioSource.PlayClipAtPoint(voice,(transform.position),5f);
			}
		}
		// void ToggleParticleEffects()
		// // {
		// 	fog.enableEmission = !fog.enableEmission;
		// 	fog2.enableEmission = !fog2.enableEmission;
		// 	fog3.enableEmission = !fog3.enableEmission;
		// 	fog4.enableEmission = !fog4.enableEmission;
		// 	fog5.enableEmission = !fog5.enableEmission;
		// 	fog6.enableEmission = !fog6.enableEmission;
		// 	fog7.enableEmission = !fog7.enableEmission;
		// 	fog8.enableEmission = !fog8.enableEmission;
		// }
	}
}
