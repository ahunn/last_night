﻿using UnityEngine;
using System.Collections;
/**
 * GameManager_PanelInstructions.cs
 *
 * Enables a panel of instructions to be visible while paused
 *
 * Works in conjuction with the GameManager_Master class
 *
 * @author (Andrew Hunn)
 * @version (Beta 1.5, 12/15/16)
 */
namespace S3{
	public class GameManager_PanelInstructions : MonoBehaviour {
		public GameObject panelInstructions;
		private GameManager_Master gameManagerMaster;

		void OnEnable(){
			SetInitialReferences();
			gameManagerMaster.GameOverEvent +=TurnOffPanelInstructions;
		}

		void OnDisable(){
			gameManagerMaster.GameOverEvent -=TurnOffPanelInstructions;
		}

		void SetInitialReferences(){
			gameManagerMaster = GetComponent<GameManager_Master>();
		}

		void TurnOffPanelInstructions(){
			if(panelInstructions != null){
				panelInstructions.SetActive(false);
			}
		}
	}
}
