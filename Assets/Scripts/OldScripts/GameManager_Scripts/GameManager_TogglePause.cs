﻿using UnityEngine;
using System.Collections;
/**
 * GameManager_TogglePause.cs
 *
 * Simulates a pause of the game
 *
 * Works in conjuction with the GameManager_Master class
 *
 * @author (Andrew Hunn)
 * @version (Beta 1.5, 12/15/16)
 */
namespace S3{
	public class GameManager_TogglePause : MonoBehaviour {
		private GameManager_Master gameManagerMaster;
		private bool isPaused;

		void OnEnable(){
			SetInitialReferences();
			gameManagerMaster.MenuToggleEvent += TogglePause;
			gameManagerMaster.InventoryUIToggleEvent += TogglePause;
		}

		void OnDisable(){
			gameManagerMaster.MenuToggleEvent -= TogglePause;
			gameManagerMaster.InventoryUIToggleEvent -= TogglePause;
		}

		void SetInitialReferences(){
			gameManagerMaster = GetComponent<GameManager_Master>();
		}

		void TogglePause(){
			if(isPaused){
				Time.timeScale = 1;
				isPaused = false;
			}else{
				Time.timeScale = 0;
				isPaused = true;
			}
		}
	}
}
