﻿using UnityEngine;
using System.Collections;
/**
 * GameManager_GoToMenuScene.cs
 *
 * Allows for player to go to menu while in game
 *
 * Works in conjuction with the GameManager_Master class
 *
 * @author (Andrew Hunn)
 * @version (Beta 1.5, 12/15/16)
 */
namespace S3{
	public class GameManager_GoToMenuScene : MonoBehaviour{
		private GameManager_Master gameManagerMaster;

		void OnEnable() {
			SetInitialReferences();
			gameManagerMaster.GoToMenuSceneEvent += GoToMenuScene;
		}

		void OnDisable(){
			gameManagerMaster.GoToMenuSceneEvent -= GoToMenuScene;
		}

		void SetInitialReferences(){
			gameManagerMaster = GetComponent<GameManager_Master>();
		}

		void GoToMenuScene(){
			Application.LoadLevel(0);
		}
	}
}
