﻿using UnityEngine;
using System.Collections;
/**
 * GameManager_References.cs
 *
 * Ease of use for references to enemies and player in game
 *
 * Works in conjuction with the GameManager_Master class
 *
 * @author (Andrew Hunn)
 * @version (Beta 1.5, 12/15/16)
 */
namespace S3{
	public class GameManager_References : MonoBehaviour{
		public string playerTag;
		public static string _playerTag;

		public string enemyTag;
		public static string _enemyTag;
		public static GameObject _player;

		void OnEnable(){
			if(playerTag == ""){
				Debug.LogWarning("Please type in the name of the player tag in GameManager_Referenes");
			}
			if(enemyTag == ""){
				Debug.LogWarning("Please type in the name of the enemy tag in GameManager_Referenes");
			}
			_playerTag = playerTag;
			_enemyTag = enemyTag;
			_player = GameObject.FindGameObjectWithTag(_playerTag);
		}
	}
}
