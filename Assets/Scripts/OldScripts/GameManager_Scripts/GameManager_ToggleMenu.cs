﻿using UnityEngine;
using System.Collections;
/**
 * GameManager_ToggleMenu.cs
 *
 * Toggles pause menu. The actual pausing is handled in GameManager_TogglePause.cs
 *
 * Works in conjuction with the GameManager_Master class
 *
 * @author (Andrew Hunn)
 * @version (Beta 1.5, 12/15/16)
 */
namespace S3{
	public class GameManager_ToggleMenu : MonoBehaviour {
		private GameManager_Master gameManagerMaster;
		public GameObject menu;

		void Start(){
			ToggleMenu();
			ToggleMenu();
		}

		void Update(){
			CheckForMenuToggleRequest();
		}

		void OnEnable(){
			SetInitialReferences();
			gameManagerMaster.GameOverEvent += ToggleMenu;
		}

		void OnDisable(){
			gameManagerMaster.GameOverEvent -= ToggleMenu;
		}

		void SetInitialReferences(){
			gameManagerMaster = GetComponent<GameManager_Master>();
		}

		void CheckForMenuToggleRequest(){
			if(Input.GetKeyUp(KeyCode.Escape) && !gameManagerMaster.isGameOver && !gameManagerMaster.isInventoryUIOn){
				ToggleMenu();
			}
		}

		void ToggleMenu(){
			if(menu != null){
				menu.SetActive(!menu.activeSelf);
				gameManagerMaster.isMenuOn = !gameManagerMaster.isMenuOn;
				gameManagerMaster.CallEventMenuToggle();
			}else{
				Debug.LogWarning("You need to assign a UI GameObject to the Toggle Menu script in the inspector.");
			}
		}
	}
}
