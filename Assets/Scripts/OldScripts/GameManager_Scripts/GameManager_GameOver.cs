﻿using UnityEngine;
using System.Collections;
/**
 * GameManager_GameOver.cs
 *
 * Enables/disables game over screen
 *
 * Works in conjuction with the GameManager_Master class
 *
 * @author (Andrew Hunn)
 * @version (Beta 1.5, 12/15/16)
 */
namespace S3{
	public class GameManager_GameOver : MonoBehaviour{
		private GameManager_Master gameManagerMaster;
		//public Time_Count tt;
		public GameObject panelGameOver;

		void OnEnable(){
			SetInitialReferences();
			gameManagerMaster.GameOverEvent += TurnOnGameOverPanel;
		}

		void OnDisable(){
			gameManagerMaster.GameOverEvent -= TurnOnGameOverPanel;
		}

		void SetInitialReferences(){
			gameManagerMaster = GetComponent<GameManager_Master>();
		}

		public void TurnOnGameOverPanel () {
			if(panelGameOver != null){
				panelGameOver.SetActive(true);
				//tt.Finished();
			}
		}
	}
}
