﻿using UnityEngine;
using System.Collections;
/**
 * GameManager_Restart.cs
 *
 * Allows for player to restart the game from pause menu
 *
 * Works in conjuction with the GameManager_Master class
 *
 * @author (Andrew Hunn)
 * @version (Beta 1.5, 12/15/16)
 */
namespace S3{
	public class GameManager_RestartLevel : MonoBehaviour{
		private GameManager_Master gameManagerMaster;

		void OnEnable(){
			SetInitialReferences();
			gameManagerMaster.RestartLevelEvent += RestartLevel;
		}

		void OnDisable(){
			gameManagerMaster.RestartLevelEvent -= RestartLevel;
		}

		void SetInitialReferences(){
			gameManagerMaster = GetComponent<GameManager_Master>();
		}

		void RestartLevel(){
			Application.LoadLevel(Application.loadedLevel);
		}
	}
}
