﻿using UnityEngine;
using System.Collections;
/**
 * GameManager_ToggleCursor.cs
 *
 * Toggles cursor state for pause meun
 *
 * Works in conjuction with the GameManager_Master class
 *
 * @author (Andrew Hunn)
 * @version (Beta 1.5, 12/15/16)
 */
namespace S3{
	public class GameManager_ToggleCursor : MonoBehaviour {
		private GameManager_Master gameManagerMaster;
		private bool isCursorLocked = true;

		void OnEnable(){
			SetInitialReferences();
			gameManagerMaster.MenuToggleEvent += ToggleCursorState;
			gameManagerMaster.InventoryUIToggleEvent += ToggleCursorState;
		}

		void OnDiable(){
			gameManagerMaster.MenuToggleEvent -= ToggleCursorState;
			gameManagerMaster.InventoryUIToggleEvent -= ToggleCursorState;
		}

		void Update(){
			CheckIfCursorShouldBeLocked();
		}

		void SetInitialReferences(){
			gameManagerMaster = GetComponent<GameManager_Master>();
		}

		void ToggleCursorState(){
			isCursorLocked = !isCursorLocked;
		}

		void CheckIfCursorShouldBeLocked(){
			if(isCursorLocked){
				Cursor.lockState = CursorLockMode.Locked;
				Cursor.visible = false;
			}else{
				Cursor.lockState = CursorLockMode.None;
				Cursor.visible = true;
			}
		}
	}
}
