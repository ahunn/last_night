using UnityEngine;
using System.Collections;

namespace S3{

public class Trigger_Master : MonoBehaviour
{
		public delegate void GeneralEventHandler(Collider hit);
		public event  GeneralEventHandler EventEnterMainDoorTrigger;
		public event  GeneralEventHandler EventEnterBathDoorTrigger;
		//public event GeneralEventHandler EventEnterRadioTrigger;
		public event GeneralEventHandler EventEnterRadioTrigger;
			//public delegate void PickupActionEventHandler(Transform item);

		public delegate void RadioEventHandler();
		public event RadioEventHandler EventRadioPlayMusic;

		public void CallEventRadioPlayMusic(){
			if(EventRadioPlayMusic!= null)
			{
				EventRadioPlayMusic();
			}
		}

		public void CallEventEnterMainDoorTrigger(Collider hit){
			if(EventEnterMainDoorTrigger!= null)
			{
				EventEnterMainDoorTrigger(hit);
			}
		}

		public void CallEventEnterBathDoorTrigger(Collider hit){
			if(EventEnterBathDoorTrigger!= null)
			{
				EventEnterBathDoorTrigger(hit);
			}
		}

		public void CallEventEnterRadioTrigger(Collider hit){
			if(EventEnterRadioTrigger!= null)
			{
				EventEnterRadioTrigger(hit);
			}
		}

	}
}
