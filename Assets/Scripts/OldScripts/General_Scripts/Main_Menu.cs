
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
namespace S3
{
	[RequireComponent(typeof(AudioSource))]
		public class Main_Menu : MonoBehaviour
		{
			private CanvasGroup canvasGroup;
			public GameObject House;
			public bool flickLights = true;
			public Main_Menu_Flicker flick;
			public AudioClip buttonMusic;
			public AudioSource buttonMusicSource;
			private float volume = 5;

			void OnEnable()
			{
			//AudioSource	buttonMusicSource = GameObject.FindObjectWithTag("Finish") .GetComponent<AudioSource>();
			//AudioSource	mainMusic = GetComponent<AudioSource>();
			}

			void Update()
			{
				if(flickLights)
				{
					flick.FlickerLights();
				}
			}
			public void PlayGame()
			{
				flickLights = false;
				GetComponent<AudioSource>().Stop();
				StartCoroutine(LoadingScreen());
				//Application.LoadLevel(1);
			}

			public void ExitGame(){
				Application.Quit();
			}

		 IEnumerator LoadingScreen()
			{
				buttonMusicSource.Play();
				AudioSource.PlayClipAtPoint(buttonMusic,transform.position,volume);
				flick.TurnOffLights();

				CanvasGroup canvasGroup = GetComponent<CanvasGroup>();

				while(canvasGroup.alpha > 0){
			 		canvasGroup.alpha-= Time.deltaTime/2;
					yield return null;
				}
				Destroy(House);
				canvasGroup.interactable = false;
				yield return null;

				yield return new WaitForSeconds(3);
			 	Application.LoadLevel(1);
			 }
		}
}
