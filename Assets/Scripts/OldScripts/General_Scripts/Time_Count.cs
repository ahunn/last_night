﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
namespace S3
{
	public class Time_Count : MonoBehaviour
	{
		public Text timerText;
		//public SpawnerProximity spawn;
		//public Spawn_Controller afterSpawns;
		private float startTime;
		private string minutes;
		private string seconds;
		private bool finished = false;
		// Use this for initialization
		void Start ()
		{
			startTime = Time.time;
		}

		// Update is called once per frame
		void Update ()
		{
			if(finished)
				return;

				float t = Time.time - startTime;
				minutes = ((int)t / 60).ToString();
				seconds = (t % 60).ToString("f2");

				timerText.text = minutes + ":" + seconds;
		}
		public void Finished()
		{
			finished = true;
			timerText.color = Color.red;
		}
	}
}
