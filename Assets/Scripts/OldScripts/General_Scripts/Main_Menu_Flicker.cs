using UnityEngine;
using System.Collections;

namespace S3
{
	public class Main_Menu_Flicker : MonoBehaviour {

 	private Light[] lights;
	private Color blueLight;
	private Color redLight;
	private Color regularLight;
	private bool isEnabled = true;
		void SetInitialReferences()
		{
					lights = FindObjectsOfType(typeof(Light)) as Light[];
					blueLight = new Color(0.8f,(224f/255f), 1f);
					redLight = new Color(1f, (60f/255f), (60f/255f));
					regularLight = new Color(1f, (253f/255f), (233f/255f));
		}

		void OnEnable()
		{
			SetInitialReferences();
		}

		public void FlickerLights()
		{
			float RandomNumber = Random.value;
			if(isEnabled == true)
			{
				if(RandomNumber <=.2)
				{
					foreach(Light light in lights)
					{
							light.intensity = 0;
					}
				}
				else
				{
					foreach(Light light in lights)
					{
							light.intensity = 7.72f;
					}
				}
			}
		}

		public void TurnOffLights()
		{
			foreach(Light light in lights)
			{
					light.intensity = 0;
			}
		}

		public void DimLights()
		{
			foreach(Light light in lights)
			{
					light.intensity = 0.56f;
			}
		}

		public void NormalLights()
		{
			foreach(Light light in lights)
			{
					light.intensity = 0.7f;
			}
		}

		public void ChangeToBlue()
		{
			foreach(Light light in lights)
			{
					light.color = blueLight;
			}
		}

		public void ChangeToRed()
		{
			foreach(Light light in lights)
			{
					light.color = redLight;
			}
		}

		public void ChangeToRegular()
		{
			foreach(Light light in lights)
			{
					light.color = regularLight;
			}
		}

		public void DimmedRange()
		{
			foreach(Light light in lights)
			{
					light.range = 10f;
			}
		}

		public void RegularRange()
		{
			foreach(Light light in lights)
			{
					light.range = 20f;
			}

		}
	}
}
