﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
/**
 * Gun_AmmoUI.cs
 *
 * Handles ammo UI for the player
 *
 * Works in conjuction with the Gun_Master and Player_Master classes
 *
 * @author (Andrew Hunn)
 * @version (Beta 1.5, 12/15/16)
 */
namespace S3{
	public class Gun_AmmoUI : MonoBehaviour {
		public InputField currentAmmoField;
		public InputField carriedAmmoField;
		private Gun_Master gunMaster;

		void OnEnable(){
			SetInitialReferences();
			gunMaster.EventAmmoChanged += UpdateAmmoUI;
		}

		void OnDisable(){
			gunMaster.EventAmmoChanged -= UpdateAmmoUI;
		}

		void SetInitialReferences(){
			gunMaster = GetComponent<Gun_Master>();
		}

		void UpdateAmmoUI(int currentAmmo, int carriedAmmo){
			if(currentAmmoField != null){
				currentAmmoField.text = currentAmmo.ToString();
			}
			if(carriedAmmoField != null)
			{
				carriedAmmoField.text = carriedAmmo.ToString();
			}
		}
	}
}
