﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
namespace S3
{
	public class Gun_ChangeColor : MonoBehaviour {

		private GameObject top;

		public void ChangeToRed()
		{
			StartCoroutine(DelayColorChange());
		}

			IEnumerator DelayColorChange()
			{
					GetComponent<Image>().color= new Color32(0,0,0,100);
					yield return new WaitForSeconds(0.5f);
					GetComponent<Image>().color= new Color32(255,0,0,255);
			}
	}
}
