using UnityEngine;
using System.Collections;

namespace S3
{
	public class Gun_ApplyDamage : MonoBehaviour {
		private Gun_Master gunMaster;
		private Gun_ChangeColor changeColor;
		public Animator crosshairAnimator;
		private GameObject top;
		public int damage = 10;

		void OnEnable()
		{
			SetInitialReferences();
			gunMaster.EventShotEnemy += ApplyDamage;
		}

		void OnDisable()
		{
			gunMaster.EventShotEnemy -= ApplyDamage;
		}

		void SetInitialReferences()
		{
			gunMaster = GetComponent<Gun_Master>();
			top = GameObject.FindGameObjectWithTag("Crosshair");
			changeColor = top.GetComponent<Gun_ChangeColor>();
		}
		void ApplyDamage(Vector3 hitPosition, Transform hitTransform)
		{
			if(hitTransform.GetComponent<Enemy_TakeDamage>() != null)
			{

				hitTransform.GetComponent<Enemy_TakeDamage>().ProcessDamage(damage);
				// changeColor.ChangeToRed();
				// //changeColor.ChangeToGreen();
				crosshairAnimator.SetTrigger("Attack");

			}
		}
	}
}
