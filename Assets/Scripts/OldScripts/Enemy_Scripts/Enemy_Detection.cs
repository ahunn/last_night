﻿using UnityEngine;
using System.Collections;
/**
 * Enemy_Detection.cs
 *
 * Enemy detection handler. Edit detect radius and check frequency
 *
 * Works in conjuction with the Enemy_Master class
 *
 * @author (Andrew Hunn)
 * @version (Beta 1.5, 12/15/16)
 */
namespace S3{
	public class Enemy_Detection : MonoBehaviour {
		private Enemy_Master enemyMaster;
		private Transform myTransform;

		public Transform head;
		public LayerMask playerLayer;
		public LayerMask sightLayer;
		private RaycastHit hit;

		private float checkRate;
		private float nextCheck;
		private float detectRadius = 360; //80


		void OnEnable(){
			SetInitialReferences();
			enemyMaster.EventEnemyDie += DisableThis;
		}

		void OnDisable(){
			enemyMaster.EventEnemyDie -= DisableThis;
		}

		void Update(){
			CarryOutDetection();
		}

		void SetInitialReferences(){
			enemyMaster = GetComponent<Enemy_Master>();
			myTransform = transform;

			if(head == null){
				head = myTransform;
			}

			//How often the enemy checks for player. Random value so enemies wouldn't look for enemy at same time
			checkRate = Random.Range(0.8f, 1f);
		}

		/**Simulates enemy field of vision**/
		void CarryOutDetection(){
			if(Time.time >nextCheck){
				nextCheck = Time.time + checkRate;

				Collider [] colliders = Physics.OverlapSphere(myTransform.position, detectRadius, playerLayer);

				if(colliders.Length > 0){
					foreach(Collider potentialTargetCollider in colliders){
						if(potentialTargetCollider.CompareTag(GameManager_References._playerTag)){  //GameManager_References._playerTag
							if(CanPotentialTargetBeSeen(potentialTargetCollider.transform)){
								break;
							}
						}
					}
				}else{
					enemyMaster.CallEventEnemyLostTarget();
				}
			}
		}

		/**If player can be seen, call set nav target event, else, call lost target event**/
		bool CanPotentialTargetBeSeen(Transform potentialTarget){
			if(Physics.Linecast(head.position, potentialTarget.position, out hit,sightLayer)){
				if(hit.transform == potentialTarget){
					enemyMaster.myTarget = potentialTarget;
					enemyMaster.CallEventEnemySetNavTarget(potentialTarget);
					return true;
				}else{
					enemyMaster.myTarget = null;
					enemyMaster.CallEventEnemyLostTarget();
					return false;
				}
			}else{
				enemyMaster.myTarget = null;
				enemyMaster.CallEventEnemyLostTarget();
				return false;
			}
		}

		void DisableThis(){
			this.enabled = false;
		}
	}
}
