﻿using UnityEngine;
using System.Collections;
/**
 * Enemy_NavPause.cs
 *
 * Handles navigation pause with enemies
 *
 * Works in conjuction with the Enemy_Master class
 *
 * @author (Andrew Hunn)
 * @version (Beta 1.5, 12/15/16)
 */
namespace S3{
	public class Enemy_NavPause : MonoBehaviour {
		private Enemy_Master enemyMaster;
		private UnityEngine.AI.NavMeshAgent myNavMeshAgent;
		private float pauseTime = 1;

		void OnEnable(){
			SetInitialReferences();
			enemyMaster.EventEnemyDie += DisableThis;
			enemyMaster.EventEnemyDeductHealth += PauseNavMeshAgent;
		}

		void OnDisable(){
			enemyMaster.EventEnemyDie -= DisableThis;
			enemyMaster.EventEnemyDeductHealth -= PauseNavMeshAgent;
		}

		void SetInitialReferences(){
			enemyMaster = GetComponent<Enemy_Master>();
			if(GetComponent<UnityEngine.AI.NavMeshAgent>()!=null){
				myNavMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
			}
		}

		void PauseNavMeshAgent(int dummy){
			if(myNavMeshAgent != null){
				if(myNavMeshAgent.enabled){
					myNavMeshAgent.ResetPath();
					enemyMaster.isNavPaused = true;
					StartCoroutine(RestartNavMeshAgent());
				}
			}
		}

		void DisableThis (){
			StopAllCoroutines();
		}

		IEnumerator RestartNavMeshAgent(){
			yield return new WaitForSeconds(pauseTime);
			enemyMaster.isNavPaused = false;
		}
	}
}
