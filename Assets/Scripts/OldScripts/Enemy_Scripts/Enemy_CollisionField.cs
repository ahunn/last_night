﻿using UnityEngine;
using System.Collections;
/**
 * Enemy_CollisionField.cs
 *
 * Enemy collision field handler. Detects player hits
 *
 * Works in conjuction with the Enemy_Master class
 *
 * @author (Andrew Hunn)
 * @version (Beta 1.5, 12/15/16)
 */
namespace S3{
	public class Enemy_CollisionField : MonoBehaviour{
		private Enemy_Master enemyMaster;
		private Rigidbody rigidBodyStrikingMe;

		private int damageToApply;
		public float massRequirement = 50;	//Mass of thrown object needed to damage
		public float speedRequirement = 5;	//Speed of object needed to damage
		private float damageFactor = 0.1f;	//Damage factor given mass and speed reached

		void OnEnable (){
			SetInitialReferences();
			enemyMaster.EventEnemyDie +=DisableThis;
		}

		void OnDisable (){
			enemyMaster.EventEnemyDie -=DisableThis;
		}
		
		void OnTriggerEnter (Collider other){
			if(other.GetComponent<Rigidbody>()!= null){

				rigidBodyStrikingMe = other.GetComponent<Rigidbody>();

				if(rigidBodyStrikingMe.mass >= massRequirement &&
						rigidBodyStrikingMe.velocity.sqrMagnitude > speedRequirement * speedRequirement){

							//Given speed and mass, calculate damage inflicted by player and deduct health from enemy
							damageToApply = (int)(damageFactor * rigidBodyStrikingMe.mass * rigidBodyStrikingMe.velocity.magnitude);
							enemyMaster.CallEventEnemyDeductHealth(damageToApply);
						}
			}
		}

		void SetInitialReferences (){
			enemyMaster = transform.root.GetComponent<Enemy_Master>();
		}

		void DisableThis (){
			gameObject.SetActive(false);
		}
	}
}
