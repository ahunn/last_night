﻿using UnityEngine;
using System.Collections;
/**
 * Enemy_TakeDamage.cs
 *
 * Processes damage inflicted by player to enemies
 *
 * Works in conjuction with the Enemy_Master class
 *
 * @author (Andrew Hunn)
 * @version (Beta 1.5, 12/15/16)
 */
namespace S3{
	public class Enemy_TakeDamage : MonoBehaviour {
		private Enemy_Master enemyMaster;
		public int damageMultiplier = 1;
		public bool shouldRemoveCollider;

		void OnEnable (){
			SetInitialReferences();
			enemyMaster.EventEnemyDie += RemoveThis;
		}

		void OnDisable (){
			enemyMaster.EventEnemyDie -= RemoveThis;
		}

		void SetInitialReferences(){
			enemyMaster = transform.root.GetComponent<Enemy_Master>();
		}

		public void ProcessDamage(int damage){
			int damageToApply = damage*damageMultiplier;
			enemyMaster.CallEventEnemyDeductHealth(damageToApply);
		}

		void RemoveThis(){
			if(shouldRemoveCollider){
				if(GetComponent<Collider>()!=null){
					Destroy(GetComponent<Collider>());
				}

				if(GetComponent<Rigidbody>() != null){
					Destroy(GetComponent<Rigidbody>());
				}
			}
			Destroy(this);
		}
	}
}
