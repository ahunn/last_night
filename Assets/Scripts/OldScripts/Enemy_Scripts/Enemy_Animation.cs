﻿using UnityEngine;
using System.Collections;
/**
 * Enemy_Animation.cs
 *
 * Enemy animation handler
 *
 * Works in conjuction with the Enemy_Master class
 *
 * @author (Andrew Hunn)
 * @version (Beta 1.5, 12/15/16)
 */
namespace S3{
	public class Enemy_Animation : MonoBehaviour{
		private Enemy_Master enemyMaster;
		private Animator myAnimator;

		void OnEnable(){
			SetInitialReferences();
			enemyMaster.EventEnemyDie += DisableAnimator;
			enemyMaster.EventEnemyWalking += SetAnimationToWalk;
			enemyMaster.EventEnemyReachedNavTarget += SetAnimationToIdle;
			enemyMaster.EventEnemyAttack += SetAnimationToAttack;
			enemyMaster.EventEnemyDeductHealth += SetAnimationToStruck;
		}

		void onDisable(){
			enemyMaster.EventEnemyDie -= DisableAnimator;
			enemyMaster.EventEnemyWalking -= SetAnimationToWalk;
			enemyMaster.EventEnemyReachedNavTarget -= SetAnimationToIdle;
			enemyMaster.EventEnemyAttack -= SetAnimationToAttack;
			enemyMaster.EventEnemyDeductHealth -= SetAnimationToStruck;
		}

		void SetInitialReferences(){
			enemyMaster = GetComponent<Enemy_Master>();
			if(GetComponent<Animator>()!= null){
				myAnimator = GetComponent<Animator>();
			}
		}

		void SetAnimationToWalk(){
			if(myAnimator != null){
				if(myAnimator.enabled){
					myAnimator.SetBool("isPursuing", true);
				}
			}
		}

		void SetAnimationToIdle(){
			if(myAnimator != null){
				if(myAnimator.enabled){
					myAnimator.SetBool("isPursuing", false);
				}
			}
		}

		void SetAnimationToAttack(){
			if(myAnimator != null){
				if(myAnimator.enabled){
					myAnimator.SetTrigger("Attack");
				}
			}
		}

		void SetAnimationToStruck(int dummy){
			if(myAnimator != null){
				if(myAnimator.enabled){
					myAnimator.SetTrigger("Struck");
				}
			}
		}

		void DisableAnimator(){
			if(myAnimator != null){
				myAnimator.enabled = false;
			}
		}
	}
}
