﻿using UnityEngine;
using System.Collections;
/**
 * Enemy_Health.cs
 *
 * Enemy health handler.
 *
 * Works in conjuction with the Enemy_Master class
 *
 * @author (Andrew Hunn)
 * @version (Beta 1.5, 12/15/16)
 */
namespace S3{
	public class Enemy_Health : MonoBehaviour {
		private Enemy_Master enemyMaster;
		public int enemyHealth = 100;	//Enemy default health
		void OnEnable(){
			SetInitialReferences();
			enemyMaster.EventEnemyDeductHealth += DeductHealth;
		}

		void OnDisable(){
			enemyMaster.EventEnemyDeductHealth -= DeductHealth;
		}

		void SetInitialReferences(){
			enemyMaster = GetComponent<Enemy_Master>();

		}

		void DeductHealth(int healthChange){
			enemyHealth -= healthChange;
			if(enemyHealth <=0){
				enemyHealth = 0;
				enemyMaster.CallEventEnemyDie();
				//Despawn enemy within 10-20 seconds upon death
				Destroy(gameObject,Random.Range(5,8));
			}
		}
			
	}
}
