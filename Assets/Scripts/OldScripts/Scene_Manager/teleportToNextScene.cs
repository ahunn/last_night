﻿using UnityEngine;
using System.Collections;

namespace S3
{
	public class teleportToNextScene: MonoBehaviour {

		private bool entered = false;
		private bool hasStarted = false;
		void SetInitialReferences(){
			this.GetComponent<Collider> ().isTrigger = true;

		}

		void OnEnable()
		{
			SetInitialReferences ();
		}

		void OnTriggerEnter(Collider other){
			if (other.tag == "Player"){
				entered = true;
			}
		}
		void OnTriggerExit(Collider other){
			if (other.tag == "Player")
				entered = false;
		}

		public bool isTriggered(){
			return entered;
		}
	}
}
