﻿using UnityEngine;
using System.Collections;

namespace S3
{
	public class bathroomTrigger: MonoBehaviour {

		private bool entered = false;
		private bool hasStarted = false;

		public GameObject bathroomDoor;
		public GameObject weaponPrefab;
		public GameObject weaponSpawnLocation;
		private AudioSource myAudioSource;
		public AudioClip murderClip;
		public AudioClip miscClip;
		public AudioClip DoorSlamClip;
		private BloodTrail trail;
		public GameObject bloodPrefab;
		public GameObject gameManager;
		private Notification_Master notification;

		private Light_Controller lights;

		void SetInitialReferences(){
			lights = gameManager.GetComponent<Light_Controller> ();
			myAudioSource = gameManager.GetComponent<AudioSource> ();
			myAudioSource.clip = DoorSlamClip;
			trail = bloodPrefab.GetComponent<BloodTrail> ();
			trail.toggle ();
			notification = gameManager.GetComponent<Notification_Master> ();
		}

		void OnEnable(){
			SetInitialReferences ();
		}
		void OnTriggerEnter(Collider other){
			if (other.tag == "Player") {
				entered = true;
				if (!hasStarted)
					StartCoroutine (sceneSequence ());
				hasStarted = true;
			}
		}
		void OnTriggerExit(Collider other){
			if (other.tag == "Player")
				entered = false;
		}

		public bool isTriggered(){
			return entered;
		}

		IEnumerator sceneSequence(){
			//Close and lock door
			//Door Animation : Triggers notification to play
			bathroomDoor.GetComponent<Animator>().Play ("Door_Close");
			bathroomDoor.GetComponent<Animator> ().ResetTrigger ("open");
			bathroomDoor.GetComponent<Animator> ().speed = 10f;
			myAudioSource.Play ();
			yield return new WaitForSeconds (DoorSlamClip.length);
			bathroomDoor.GetComponent<Animator> ().enabled = false;
			//Start audio
			myAudioSource.clip = murderClip;
			myAudioSource.Play ();
			Debug.Log ("Lighting ");
			//Dim Lighting
			lights.changeState();
			//Spawn weapon
			Debug.Log ("spawn ");
			Instantiate(weaponPrefab, weaponSpawnLocation.transform.TransformPoint(Vector3.forward), Quaternion.identity);
			Debug.Log ("trail ");
			//Enable blood trail
			trail.toggle();
			//Adjust lighting
			lights.changeState();
			lights.NormalLights();
			lights.ToggleFirstFloor(true);
			lights.ToggleSecondFloor (false);
			yield return new WaitForSeconds (murderClip.length);
			Debug.Log ("animator ");
			//Unlock door
			bathroomDoor.GetComponent<Animator> ().enabled = true;
			bathroomDoor.GetComponent<Animator>().Play ("Door_open");
			bathroomDoor.GetComponent<Animator> ().SetTrigger ("open");
			bathroomDoor.GetComponent<Animator> ().speed = 1f;
			//Quick laugh audio
			notification.ChangeNotificaiton ("Pick up weapon.");

		}
	}
}
