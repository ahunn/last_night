﻿using UnityEngine;
using System.Collections;

namespace S3
{
	public class openDaughterDoor: MonoBehaviour {

		private bool entered = false;
		void SetInitialReferences(){
			this.GetComponent<Collider> ().isTrigger = true;

		}

		void OnEnable()
		{
			SetInitialReferences ();
		}

		void OnTriggerEnter(Collider other){
			if (other.tag == "Player")
				entered = true;
		}
		void OnTriggerExit(Collider other){
			if (other.tag == "Player")
				entered = false;
		}

		public bool isTriggered(){
			return entered;
		}
	}
}
