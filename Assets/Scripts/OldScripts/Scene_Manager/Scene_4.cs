﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;
namespace S3
{
	public class Scene_4: MonoBehaviour {

		public GameObject playerPrefab;
		//Bathroom

		public GameObject bathroomDoor;
		public GameObject doorArea;
		private openBathroomDoor bathroomDoorTrigger;

		//LivingRoom
		public GameObject blockHallway;

		//Scripts
		private Light_Controller lights;
		private FirstPersonController fpc;
		private Scene_Master sceneMaster;

		//Audio
		private AudioSource myAudioSource; 

		void OnEnable (){
			SetInitialReferences ();
			sceneStart ();
		}
		void sceneStart(){

			StartCoroutine (sceneSequence ());
		}


		void SetInitialReferences(){
			fpc = playerPrefab.GetComponent<FirstPersonController> ();
			bathroomDoorTrigger = doorArea.GetComponent<openBathroomDoor> ();
			//bathroomDoor.GetComponent<Animator>().Play ("Door_Close");
			bathroomDoor.GetComponent<Animator> ().ResetTrigger ("open");
			blockHallway.GetComponent<Collider> ().isTrigger = false;

		}

		//This method disables any uneeded trigger events as well as any gameobject cleanup and door locks 
		void sceneExit(){
			blockHallway.GetComponent<Collider> ().isTrigger = true;
			sceneMaster.sceneIncrement ();
		}


		IEnumerator sceneSequence(){
			yield return new WaitForSeconds (1);


			//Audio plays
			//Horde mode initalizes
			//Kill all enemies, area will be set up with ammo and health, but not much
			//Once 31 enemies have been defeated, door appears infront of you
			//Voice tells you to do it
			//Going through door will switch player back to house. Final scene comences
		}

	}
}
