﻿using UnityEngine;
using System.Collections;

namespace S3
{
	public class livingRoomTrigger: MonoBehaviour {

		private bool entered = false;
		private bool hasStarted = false;

		public Collider livingRoomBlock;
		public Collider livingRoomBlock2;
		public GameObject nightmarePrefab;

		public GameObject nightmareSpawnLocation;
		public GameObject bloodDoor;
		private Spawn_Controller spawn;
		private atticDoorTeleport adt;
		private AudioSource myAudioSource;
		public AudioClip newsClip;
		public AudioClip voiceClip;
		public AudioClip screamClip;
		public AudioClip gunshotClip;
		public GameObject playerPrefab;

		public GameObject gameManager;
		private Notification_Master notification;

		private Light_Controller lights;

		void SetInitialReferences(){
			adt = bloodDoor.GetComponent<atticDoorTeleport> ();
			lights = gameManager.GetComponent<Light_Controller> ();
			myAudioSource = gameManager.GetComponent<AudioSource> ();
			myAudioSource.clip = voiceClip;
			spawn = nightmareSpawnLocation.GetComponent<Spawn_Controller> ();
			notification = gameManager.GetComponent<Notification_Master> ();
		}

		void OnEnable(){
			SetInitialReferences ();
		}
		void OnTriggerEnter(Collider other){
			if (other.tag == "Player") {
				entered = true;
				if (!hasStarted)
					StartCoroutine (sceneSequence ());
			}
		}
		void OnTriggerExit(Collider other){
			if (other.tag == "Player")
				entered = false;
		}

		public bool isTriggered(){
			return entered;
		}

		IEnumerator sceneSequence(){
			//Close and lock door
			//Door Animation : Triggers notification to play
			hasStarted = true;
			livingRoomBlock.isTrigger = false;
			livingRoomBlock2.isTrigger = false;
			myAudioSource.Play ();
			yield return new WaitForSeconds (voiceClip.length);
			myAudioSource.clip = newsClip;
			myAudioSource.Play ();
			spawn.spawnEnemy (nightmarePrefab, nightmareSpawnLocation, 3);	//PREFAB AND NUMBER PARAMS

			notification.ChangeNotificaiton ("Defeat the creatures.");

			//Once player kills all nightmare clones, play audio and unlock living room
			while (GameObject.Find("Nightmare(Clone)") == true)
				yield return null;
			
			myAudioSource.clip = screamClip;
			myAudioSource.Play ();
			yield return new WaitForSeconds (screamClip.length);

			myAudioSource.clip = gunshotClip;
			myAudioSource.Play ();
			yield return new WaitForSeconds (gunshotClip.length);

			//Unlock room
			livingRoomBlock.isTrigger = true;
			livingRoomBlock2.isTrigger = true;

			bloodDoor.SetActive (true);
			notification.ChangeNotificaiton ("Investigate the Bloody Door.");
			while (adt.isTriggered () == false)
				yield return null;
			adt.transport (playerPrefab, myAudioSource, screamClip, gameManager);
			//bloodDoor.SetActive (false);

			//Update notification


		}
	}
}
