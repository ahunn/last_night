﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

namespace S3
{
	public class atticDoorTeleport: MonoBehaviour {

		private bool entered = false;
		void SetInitialReferences(){


		}

		void OnTriggerEnter(Collider other){
			if (other.tag == "Player")
				entered = true;
		}
		void OnTriggerExit(Collider other){
			if (other.tag == "Player")
				entered = false;
		}

		public bool isTriggered(){
			return entered;
		}

		public void transport(GameObject player, AudioSource myAudio, AudioClip clip, GameObject gameManager){
			player.GetComponent<FirstPersonController>().enabled = false;
			StartCoroutine (audioOver (myAudio,clip,gameManager));

		}

		IEnumerator audioOver(AudioSource myAudio, AudioClip clip, GameObject gameManager){
			myAudio.clip = clip;
			myAudio.Play ();
			gameManager.GetComponent<Scene_Master> ().fadeOut ();
			yield return new WaitForSeconds (clip.length);
			Application.LoadLevel (2);


		}
	}
}
