﻿using UnityEngine;
using System.Collections;

namespace S3
{
	public class Scene_Master: MonoBehaviour {

		private Scene_1 s1;
		private Scene_2 s2;
		private Scene_3 s3;
		public int counter = 0; 
		private bool toggle = false;

		private Fade CanvasFade;
		public Canvas canvasFade;


		void SetInitialReferences(){
			s1 = this.GetComponent<Scene_1> ();
			s2 = this.GetComponent<Scene_2> ();
			s3 = this.GetComponent<Scene_3> ();
			s1.enabled = true;
			s2.enabled = false;
			s3.enabled = false;
		}


		void Update(){
			if (counter == 1) {
				switchScene1 ();
				//counter++;
			} else if (counter == 2) {
				switchScene2 ();
				//counter++;
			} else if (counter == 3) {
				switchScene3 ();
				//counter++;
			}
		}

		//Fade in with audio
		void Start(){
			SetInitialReferences ();

		}


		void switchScene1(){
			s1.enabled = false;
			s2.enabled = true;
		}

		void switchScene2(){
			s2.enabled = false;
			s3.enabled = true;
		}

		void switchScene3(){
			s3.enabled = false;
			//s2.enabled = true;
		}

		public void sceneIncrement()
		{
			counter++;
		}

		public void fadeOut(){
			Debug.Log ("Fading");
		}
	}
}
