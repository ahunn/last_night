﻿using UnityEngine;
using System.Collections;

namespace S3
{
	public class CribTrigger: MonoBehaviour {

		private bool entered = false;
		void SetInitialReferences(){


		}

		void OnTriggerEnter(Collider other){
			if (other.tag == "Player")
				entered = true;
		}
		void OnTriggerExit(Collider other){
			if (other.tag == "Player")
				entered = false;
		}

		public bool isTriggered(){
			return entered;
		}
	}
}
