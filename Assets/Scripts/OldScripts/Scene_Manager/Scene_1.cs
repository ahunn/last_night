﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

namespace S3
{
	public class Scene_1: MonoBehaviour {

		//Manager
		private bool sceneFinished = false;
		private bool hasEnteredTrigger = false;

		//Colliders
		public Collider stairCollider;

		//GameObjects
		public GameObject playerPrefab;
		public GameObject daughterPrefab;
		public GameObject nightmarePrefab;
		public GameObject atticDoor;
		public GameObject cribPrefab;
		public GameObject sceneAdvanceDoor;
		public GameObject atticStairCollider;
		public GameObject daughterSpawnPoint;
		public GameObject demonSpawnPoint;
		public GameObject doorExit;

		//Scripts
		private FirstPersonController fpc;
		private Scene_Master sceneMaster;
		private Light_Controller lights;
		private BloodTrail blood;
		private CribTrigger cribT;
		private atticStair aS;
		private atticDoorTeleport adt;
		private Notification_Master notification;

		//Audio
		private AudioSource myAudioSource;
		public AudioClip ConversationAudio;
		public AudioClip ScreamAudio;
		public AudioClip DoorSlamAudio;
		public AudioClip GrowlAudio;
		public AudioClip RemarkAudio;
		public AudioClip AttackAudio;

		void OnEnable(){
			SetInitialReferences ();
			sceneStart ();
		}

		void SetInitialReferences(){
			//Initalize all the scripts
			sceneMaster = this.GetComponent<Scene_Master> ();
			lights = this.GetComponent<Light_Controller> ();
			notification = this.GetComponent<Notification_Master> ();

			aS = atticStairCollider.GetComponent<atticStair> ();
			cribT = cribPrefab.GetComponent<CribTrigger> ();
			adt = sceneAdvanceDoor.GetComponent<atticDoorTeleport> ();

			myAudioSource = this.GetComponent<AudioSource> ();

			fpc = playerPrefab.GetComponent<FirstPersonController> ();
			fpc.enabled = false;

			sceneAdvanceDoor.SetActive(false);

		}

		void sceneStart(){
			StartCoroutine (sceneSequence ());
		}

		//This method disables any uneeded trigger events as well as any gameobject cleanup and door locks
		void sceneExit(){
			stairCollider.isTrigger = true;
			atticDoor.GetComponent<Animator> ().enabled = true;
			daughterPrefab.SetActive(false);
			sceneAdvanceDoor.SetActive(false);
			sceneMaster.sceneIncrement ();
		}

		IEnumerator sceneSequence(){
			//Toggle first floor lights to improve run time
			lights.ToggleFirstFloor(false);
			lights.ToggleSecondFloor (true);
			//daughterPrefab.GetComponent<Animator> ().Play ("Unarmed-Fall");
			//Initalize demon and deactivate for now
			StartCoroutine (Move (nightmarePrefab, demonSpawnPoint, 2f));
			nightmarePrefab.SetActive (false);

			//Audio	will include : Knock, reply, door opens, then conversation
			//Tinker with time to have flickering lights within the converation
			myAudioSource.clip = ConversationAudio;
			myAudioSource.Play ();
			yield return new WaitForSeconds(ConversationAudio.length - 5);

			//Flicker Lights
			lights.changeState();
			yield return new WaitForSeconds(5);

			//Transform Daughter, Scream audio includes both child and father
			myAudioSource.clip = ScreamAudio;
			myAudioSource.Play ();
			StartCoroutine (Move (daughterPrefab, atticDoor, 5f));
			yield return new WaitForSeconds(ScreamAudio.length);

			//Stop flickering and second floor lights on by default, first should remain off.
			lights.changeState ();
			lights.NormalLights ();
			lights.ToggleFirstFloor(false);
			lights.ToggleSecondFloor (true);

			//Enable player control and update objective
			fpc.enabled = true;
			notification.ChangeNotificaiton ("Investigate the attic.");

			//Door Animation : Triggers notification to play
			atticDoor.GetComponent<Animator>().Play ("Door_Close");
			atticDoor.GetComponent<Animator> ().ResetTrigger ("open");
			atticDoor.GetComponent<Animator> ().speed = 10f;

			//Transform Daughter to crib
			StartCoroutine (Move (daughterPrefab, daughterSpawnPoint, 2f));

			//Wait until the trigger is activated
			while (aS.isTriggered () == false)
				yield return null;

			//Audio	Switch to door slam, close and lock door.
			myAudioSource.clip = DoorSlamAudio;
			myAudioSource.Play ();
			atticDoor.GetComponent<Animator>().Play ("Door_Close");
			atticDoor.GetComponent<Animator> ().ResetTrigger ("open");
			atticDoor.GetComponent<Animator> ().speed = 10f;
			yield return new WaitForSeconds (DoorSlamAudio.length);
			atticDoor.GetComponent<Animator> ().enabled = false;

			//Update objective
			notification.ChangeNotificaiton ("Find a way out.");

			//Wait until crib trigger is activated
			while (cribT.isTriggered () == false)
				yield return null;

			//Disable player control
			fpc.enabled = false;

			//Position player properly
			//Transform

			//Audio changes, daughter deactivaes, demon activates
			myAudioSource.clip = AttackAudio;
			myAudioSource.Play ();
			lights.changeState ();
			daughterPrefab.SetActive(false);
			nightmarePrefab.SetActive(true);
			nightmarePrefab.GetComponent<Animator> ().enabled = true;
			nightmarePrefab.GetComponent<Animator>().Play ("Idle");
			yield return new WaitForSeconds(AttackAudio.length);

			//Light flicker
			lights.changeState ();
			lights.NormalLights ();
			lights.ToggleFirstFloor(false);
			lights.ToggleSecondFloor (true);

			//Blood textures enabled
			//Enable blood

			//Switch to growl sound effect
			myAudioSource.clip = GrowlAudio;
			myAudioSource.Play ();

			//Lights off to add suspense to disapearing creature
			lights.TurnOffLights();
			nightmarePrefab.SetActive (false);
			yield return new WaitForSeconds(GrowlAudio.length);

			//Lights on
			lights.NormalLights ();
			lights.ToggleFirstFloor(false);
			lights.ToggleSecondFloor (true);

			//Door appears
			sceneAdvanceDoor.SetActive(true);
			sceneAdvanceDoor.GetComponent<Animator> ().ResetTrigger ("open");

			//Enable player control
			fpc.enabled = true;

			//Play remark audio
			myAudioSource.clip = RemarkAudio;
			myAudioSource.Play ();

			//Update objective
			notification.ChangeNotificaiton ("Interact with strange door.");

			//Once player interacts with door, sceneExit
			while (adt.isTriggered () == false)
				yield return null;

			//Transform player to other part of house. Clean up scene 1, start scene 2
			playerPrefab.transform.position = doorExit.transform.position + doorExit.transform.forward * 1;
			playerPrefab.transform.rotation = Quaternion.identity;
			sceneExit ();
		}

		IEnumerator Move(GameObject x, GameObject y, float speed){
			while (x.transform.position != y.transform.position) {
				x.transform.position =
					Vector3.MoveTowards (x.transform.position, y.transform.position, speed * Time.deltaTime);
				yield return new WaitForEndOfFrame ();
			}
		}
	}
}
