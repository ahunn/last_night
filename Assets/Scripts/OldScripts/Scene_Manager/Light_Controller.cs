﻿using UnityEngine;
using System.Collections;

namespace S3
{
	public class Light_Controller : MonoBehaviour {

 	private Light[] lights;
	private Color blueLight;
	private Color redLight;
	private Color regularLight;
	private bool isEnabled = true;
	private bool flicker = false;
		void SetInitialReferences(){
			lights = FindObjectsOfType(typeof(Light)) as Light[];
			blueLight = new Color(0.8f,(224f/255f), 1f);
			redLight = new Color(1f, (60f/255f), (60f/255f));
			regularLight = new Color(1f, (253f/255f), (233f/255f));
		}

		void OnEnable()
		{
			SetInitialReferences();
		}


		void Update()
		{
			if (flicker)
				FlickerLights ();
			
		
				
		}
		public void FlickerLights()
		{
			float RandomNumber = Random.value;
			if(flicker == true)
			{
				if(RandomNumber <=.5)
				{
					foreach(Light light in lights)
					{
							light.intensity = 0;
					}
				}
				else
				{
					foreach(Light light in lights)
					{
							light.intensity = 0.6f;
					}
				}
			}
		}

		public void changeState()
		{
			flicker = !flicker;
		}

		//Method for final StartSequence
		//Method to change color of lights

		//Method to Method to change brightness of lights

		public void TurnOffLights()
		{
			foreach(Light light in lights)
			{
					light.intensity = 0;
			}
		} 

		public void DimLights()
		{
			foreach(Light light in lights)
			{
					light.intensity = 0.56f;
			}
		}

		public void NormalLights()
		{
			foreach(Light light in lights)
			{
					light.intensity = 0.6f;
			}
		}

		public void ChangeToBlue()
		{
			foreach(Light light in lights)
			{
					light.color = blueLight;
			}
		}

		public void ChangeToRed()
		{
			foreach(Light light in lights)
			{
					light.color = redLight;
			}
		}

		public void ChangeToRegular()
		{
			foreach(Light light in lights)
			{
					light.color = regularLight;
			}
		}

		public void DimmedRange()
		{
			foreach(Light light in lights)
			{
					light.range = 10f;
			}
		}

		public void RegularRange()
		{
			foreach(Light light in lights)
			{
					light.range = 6.18f;
			}

		}

		public void ToggleFirstFloor(bool value){

			foreach(Light light in lights)
			{
				if (light.name == "First Floor Point light")
					light.enabled = value;
			}
		}

		public void ToggleSecondFloor(bool value){

			foreach(Light light in lights)
			{
				if (light.name == "Second Floor Point light")
					light.enabled = value;
			}
		}

		public void ToggleFirstFloorNoParam(){

			foreach(Light light in lights)
			{
				if (light.name == "First Floor Point light")
					light.enabled = !light.enabled;
			}
		}

		public void ToggleSecondFloorNoParam(){

			foreach(Light light in lights)
			{
				if (light.name == "Second Floor Point light")
					light.enabled = !light.enabled;
			}
		}


	}
}
