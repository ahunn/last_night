﻿using UnityEngine;
using System.Collections;

namespace S3
{
	public class daughterRoomTrigger: MonoBehaviour {

		private bool entered = false;
		private bool hasStarted = false;

		public GameObject nightmarePrefab;
		public GameObject teddyBear;


		public GameObject nightmareSpawnLocation;
		private InteractScript playerInteract;
		public GameObject playerPrefab;
		private Spawn_Controller spawn;
		public GameObject daughterDoor;
		public GameObject nightmareTeddyBear;


		private AudioSource myAudioSource;
		public AudioClip voiceClip;
		public AudioClip daughterClip;
		public AudioClip screamClip;
		public AudioClip DoorSlamClip;

		public GameObject gameManager;
		private Notification_Master notification;

		private Light_Controller lights;

		void SetInitialReferences(){
			lights = this.GetComponent<Light_Controller> ();
			myAudioSource = this.GetComponent<AudioSource> ();
			myAudioSource.clip = DoorSlamClip;
			spawn = nightmareSpawnLocation.GetComponent<Spawn_Controller> ();
			notification = gameManager.GetComponent<Notification_Master> ();
			playerInteract = playerPrefab.GetComponent<InteractScript> ();
		}

		void OnEnable(){
			SetInitialReferences ();
		}
		void OnTriggerEnter(Collider other){
			if (other.tag == "Player") {
				entered = true;
				if (!hasStarted)
					StartCoroutine (sceneSequence ());
			}
		}
		void OnTriggerExit(Collider other){
			if (other.tag == "Player")
				entered = false;
		}

		public bool isTriggered(){
			return entered;
		}

		bool findChildFromParent(string parentName, string childNameToFind){
			string childLocation = "/" + parentName + "/" + childNameToFind;
			return GameObject.Find (childLocation);
		}
		IEnumerator sceneSequence(){
			//Door Animation : Triggers notification to play
			daughterDoor.GetComponent<Animator>().Play ("Door_Close");
			daughterDoor.GetComponent<Animator> ().ResetTrigger ("open");
			daughterDoor.GetComponent<Animator> ().speed = 10f;
			myAudioSource.Play ();
			yield return new WaitForSeconds (DoorSlamClip.length);
			daughterDoor.GetComponent<Animator> ().enabled = false;
			myAudioSource.clip = daughterClip;
			myAudioSource.Play ();

			//	//PREFAB AND NUMBER PARAMS

			notification.ChangeNotificaiton ("Find any clues as to where Sara is.");
		
			//Will execute rest of code when teddy bear is within player inventory
			while (findChildFromParent("Player", "Teddy Bear") == false)
				yield return null;

			while (GameObject.Find ("Teddy Bear Nightmares").activeSelf == true) {
				if (Physics.Raycast (playerPrefab.transform.position, playerPrefab.transform.TransformDirection (Vector3.forward))) {

					StartCoroutine (Move (nightmareTeddyBear, playerPrefab, 2f));
					yield return new WaitForSeconds (3);
					myAudioSource.clip = voiceClip;
					myAudioSource.Play ();
					lights.changeState ();
					yield return new WaitForSeconds (voiceClip.length);
					lights.changeState ();
					nightmareTeddyBear.SetActive (false);
					spawn.spawnEnemy (nightmarePrefab, nightmareTeddyBear, 1);
				}
			}

			//spawn.spawnEnemy (nightmarePrefab, nightmareSpawnLocation, 3);
			//Once player kills all nightmare clones, play audio and unlock living room
			while (GameObject.Find("Nightmare (clone)") == true)
				yield return null;

			myAudioSource.clip = screamClip;
			myAudioSource.Play ();
			yield return new WaitForSeconds (screamClip.length);
			daughterDoor.GetComponent<Animator> ().enabled = true;
			//Update notification
			notification.ChangeNotificaiton ("Exit the room.");


			//Teleport player to basement

		}


		IEnumerator Move(GameObject x, GameObject y, float speed){
			while (x.transform.position != y.transform.position) {
				x.transform.position =
					Vector3.MoveTowards (x.transform.position, y.transform.position, speed * Time.deltaTime);
				yield return new WaitForEndOfFrame ();
			}
		}
	}
}
