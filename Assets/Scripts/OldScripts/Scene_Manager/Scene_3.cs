﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;
namespace S3
{
	public class Scene_3: MonoBehaviour {

		public GameObject playerPrefab;
		private FirstPersonController fpc;

		public GameObject bloodDoorPrefab;
		public GameObject nightmarePrefab;
		public GameObject spawnArea1;
		public GameObject spawnArea2;
		public GameObject spawnArea3;
		public GameObject spawnArea4;


		public GameObject doorTrigger;
		private teleportToNextScene teleportToForestTrigg;

		//Scripts
		private Light_Controller lights;
		private Scene_Master sceneMaster;

		//Audio
		private AudioSource myAudioSource; 
		public AudioClip voiceClip;
		public AudioClip laughClip;

		void OnEnable (){
			SetInitialReferences ();
			sceneStart ();
		}
		void sceneStart(){

			StartCoroutine (sceneSequence ());
		}


		void SetInitialReferences(){
			fpc = playerPrefab.GetComponent<FirstPersonController> ();
			teleportToForestTrigg = doorTrigger.GetComponent<teleportToNextScene> ();
			lights = this.GetComponent<Light_Controller> ();
			sceneMaster = this.GetComponent<Scene_Master> ();
			bloodDoorPrefab.SetActive (false);

		}

		//This method disables any uneeded trigger events as well as any gameobject cleanup and door locks 
		void sceneExit(){
			//blockHallway.GetComponent<Collider> ().isTrigger = true;
			sceneMaster.sceneIncrement ();
		}


		IEnumerator sceneSequence(){
			yield return new WaitForSeconds (1);


			//Blood soaked door apears somewhere in the house
			//Notification asking to find door like the voice says
			//Nightmares keep spawning at 3 locations until found
			//Once found, scene transition to forest
		}

	}
}
