﻿using UnityEngine;
using System.Collections;

namespace S3
{
	public class floorLightToggle: MonoBehaviour {
		private Light_Controller lights;
		private bool entered = false;
		void SetInitialReferences(){
			this.GetComponent<Collider> ().isTrigger = true;
			lights = this.GetComponent<Light_Controller> ();

		}

		void OnEnable()
		{
			SetInitialReferences ();
		}

		void OnTriggerEnter(Collider other){
			if (other.tag == "Player") {
				entered = true;
				lights.ToggleFirstFloorNoParam ();
				lights.ToggleSecondFloorNoParam ();
			}
		}
		void OnTriggerExit(Collider other){
			if (other.tag == "Player")
				entered = false;
		}

		public bool isTriggered(){
			return entered;
		}
	}
}
