using UnityEngine;
using System.Collections;
namespace S3{
public class InteractScript : MonoBehaviour {
	public float interactDistance = 5f;
	private bool hasBasementTriggered = false;
		private int choiceNum = 0;
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Mouse0)) {
			Ray ray = new Ray (transform.position, transform.forward);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit, interactDistance)) {
				if (hit.collider.CompareTag ("Door")) {
					Debug.Log ("Door Found");
					hit.collider.transform.GetComponent<Animator> ().speed = 1f;
					hit.collider.transform.GetComponent<Animator> ().SetTrigger ("open");
					hit.collider.transform.GetComponent<Animator> ().Play ("Door_open");

				}
			}


		}
		if (hasBasementTriggered) {
			if (Input.GetKeyDown (KeyCode.M)) {
				Ray ray = new Ray (transform.position, transform.forward);
				RaycastHit hit;
				if (Physics.Raycast (ray, out hit, interactDistance)) {
					if (hit.collider.CompareTag ("BasementTrigger")) {
						Debug.Log ("ChoiceA");
							choiceNum = 1;
						//hit.collider.transform.GetComponent<Animator> ().speed = 1f;
						//hit.collider.transform.GetComponent<Animator> ().SetTrigger ("open");
						//hit.collider.transform.GetComponent<Animator> ().Play ("Door_open");

					}
				}
			}else if (Input.GetKeyDown (KeyCode.N)) {
				Ray ray = new Ray (transform.position, transform.forward);
				RaycastHit hit;
				if (Physics.Raycast (ray, out hit, interactDistance)) {
					if (hit.collider.CompareTag ("BasementTrigger")) {
						Debug.Log ("ChoiceB");
							choiceNum = 2;
						//hit.collider.transform.GetComponent<Animator> ().speed = 1f;
						//hit.collider.transform.GetComponent<Animator> ().SetTrigger ("open");
						//hit.collider.transform.GetComponent<Animator> ().Play ("Door_open");

					}
				}
			}
	}



}
	public bool toggleBasementEvent(){
		hasBasementTriggered = !hasBasementTriggered;
		return hasBasementTriggered;
	}
		public int choice(){

			return choiceNum;
		}

		public void basementChoice(Light_Controller lights, Animator daughterAnimate, GameObject blood, 
									AudioClip audioClip, AudioSource myAudioSource){
			//lights.TurnOffLights ();
			//daughterAnimate.Play("

		}

	}
}
