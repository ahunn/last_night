﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;
namespace S3
{
	public class Scene_2: MonoBehaviour {





		//GameObjects
		public GameObject playerPrefab;
		public GameObject basementCollider;
		public GameObject bathroomDoor;
		public GameObject doorArea;
		public GameObject daughterPrefab;
		public GameObject motherPrefab;
		public GameObject lightTransition;
	
		//LivingRoom
		public GameObject blockHallway;

		//Scripts
		private Light_Controller lights;
		private FirstPersonController fpc;
		private Scene_Master sceneMaster;
		private basementTrigger basementArea;
		private openBathroomDoor bathroomDoorTrigger;
		private Notification_Master notification;
		private InteractScript playerInteract;
		private basementExitTrigger bet;
		public GameObject exitTrigger;
		public GameObject nightmarePrefab;
		public GameObject bloodFountain;
		public GameObject daughterSpawn;
		public GameObject motherSpawn;

		private bool playerSelectedM = false;
		private bool playerSelectedN = false;

		//Audio
		private AudioSource myAudioSource;
		public AudioClip laughClip;
		public AudioClip choiceClip;
		public AudioClip miscClip;

		void OnEnable (){
			SetInitialReferences ();
			sceneStart ();

		}
		void sceneStart(){
			StartCoroutine (sceneSequence ());
		}


		void SetInitialReferences(){
			
			basementArea = basementCollider.GetComponent<basementTrigger> ();
			notification = this.GetComponent<Notification_Master> ();
			fpc = playerPrefab.GetComponent<FirstPersonController> ();
			bathroomDoorTrigger = doorArea.GetComponent<openBathroomDoor> ();
			bet = exitTrigger.GetComponent<basementExitTrigger> ();
			//bathroomDoor.GetComponent<Animator> ().enabled = false;
			//bathroomDoor.GetComponent<Animator> ().ResetTrigger ("open");
			blockHallway.GetComponent<Collider> ().isTrigger = false;
			sceneMaster = this.GetComponent<Scene_Master> ();
			myAudioSource = this.GetComponent<AudioSource> ();
			lights = this.GetComponent<Light_Controller> ();
			lights.ToggleFirstFloor(true);
			lights.ToggleSecondFloor (false);
			playerInteract = playerPrefab.GetComponent<InteractScript> ();
			exitTrigger.SetActive (false);
		}

		//This method disables any uneeded trigger events as well as any gameobject cleanup and door locks
		public void sceneExit(){
			
			sceneMaster.sceneIncrement ();
		}


		IEnumerator sceneSequence(){
			
			bathroomDoor.GetComponent<Animator> ().Play ("Door_Close");
			bathroomDoor.GetComponent<Animator> ().ResetTrigger ("open");
			bathroomDoor.GetComponent<Animator> ().speed = 999f;
			Debug.Log ("Entered");
			notification.ChangeNotificaiton ("Find Sara.");
			myAudioSource.clip = laughClip;
			myAudioSource.Play ();
			//Activate bathroom trigger. Events handled seperately
			while (bathroomDoorTrigger.isTriggered () == false)
				yield return null;
			bathroomDoor.GetComponent<Animator> ().speed = 1f;
			bathroomDoor.GetComponent<Animator> ().enabled = true;
			bathroomDoor.GetComponent<Animator>().SetTrigger ("open");
			blockHallway.GetComponent<Collider> ().isTrigger = true;
		

			//while (basementArea.isTriggered () == false)
				//yield return null;
			/*
			//Light Flickering
			lights.changeState();
			yield return new WaitForSeconds (3);
			motherPrefab.SetActive (true);
			motherPrefab.GetComponent<Animator> ().Play ("Unarmed-Fall");
			daughterPrefab.SetActive (true);
			daughterPrefab.GetComponent<Animator> ().Play ("Unarmed-Fall");
			StartCoroutine (Move (motherPrefab, motherSpawn, 10f));
			StartCoroutine (Move (daughterSpawn, daughterSpawn, 10f));
			bloodFountain.SetActive (true);

			//Enable disable player control
			fpc.enabled = false;
			//Transform to right position


			//Audio deciding to save the mother and daughter
			myAudioSource.clip = choiceClip;
			myAudioSource.Play();
			yield return new WaitForSeconds(choiceClip.length);
			notification.ChangeNotificaiton("Make your choice.");
			fpc.enabled = true;
			while (playerInteract.choice() == 0)
				yield return null;
			if (playerInteract.choice()== 1) {
				lights.TurnOffLights ();
				myAudioSource.clip = laughClip;
				yield return new WaitForSeconds (laughClip.length);
				daughterPrefab.SetActive (false);
				motherPrefab.SetActive (false);
				Instantiate (nightmarePrefab, daughterPrefab.transform.position - (transform.forward * 1), daughterPrefab.transform.rotation);
				notification.ChangeNotificaiton("Kill the demon");

			} else if (playerInteract.choice()== 2) {
				lights.TurnOffLights ();
				myAudioSource.clip = miscClip;
				yield return new WaitForSeconds (miscClip.length);
				daughterPrefab.SetActive (false);
				motherPrefab.SetActive (false);
				Instantiate (nightmarePrefab, daughterPrefab.transform.position - (transform.forward * 1), daughterPrefab.transform.rotation);
				notification.ChangeNotificaiton("Kill the demon again.");
			}

			//if(playerSelectM) then lights turn off, laugh, move models, replalce with nightmares
			//else if(playerSelectN) then lights turn off, good choice, daughter attacks with strength.

			//Once event over, notification 
			//Once player kills all nightmare clones, play audio and unlock living room
			while (GameObject.Find("Nightmare(Clone)") == true)
				yield return null;

			//Audio changing depending on choice
			notification.ChangeNotificaiton("Find this door");
			exitTrigger.SetActive (true);
			//Triggers appear: Walk forward and it is condidered saving
			//				   Walk backwards and it is considered letting them die
			//Saving them causes them to turn into nightmares
			//Letting them die, their ridgid bodies activate causing them to collapse
			//Voice saying to find the door
			while (bet.isTriggered () == false)
				yield return null;
			//Once player gets to top of stairs, house opens up, stage 3 activates.
			*/
			//sceneExit ();
		}
		IEnumerator Move(GameObject x, GameObject y, float speed){
			while (x.transform.position != y.transform.position) {
				x.transform.position =
					Vector3.MoveTowards (x.transform.position, y.transform.position, speed * Time.deltaTime);
				yield return new WaitForEndOfFrame ();
			}
		}

	}
}
