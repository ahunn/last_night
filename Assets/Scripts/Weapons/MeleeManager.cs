﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeManager : MeleeBase {

    private Transform myTransform;
    private float nextSwing;

    private float nextSwingTime;
    public int damage = 25;

    [Header("Audio")]
    public AudioClip swingSound;
    public AudioClip strikeSound;
    public float swingSoundVolume = 0.5f;
    public float strikeSoundVolume = 0.5f;

    public Collider myCollider;
    public Rigidbody myRigidbody;
    public Animator myAnimator;

    [Header("Hit Effects")]
    public GameObject defaultHitEffect;
    public GameObject enemyHitEffect;

    void Awake()
    {
        SetInitialReferences();
    }
    void OnEnable()
    {
        EventHit += PlayStrikeSound;
        EventPlayerInput += MeleeAttackActions;
        EventHit += SpawnHitEffect;

        if (GetComponent<ItemManager>() != null)
            GetComponent<ItemManager>().EventObjectThrow += ResetMelee;
    }
    void OnDisable()
    {
        EventHit -= PlayStrikeSound;
        EventPlayerInput -= MeleeAttackActions;
        EventHit -= SpawnHitEffect;

        if (GetComponent<ItemManager>() != null)
            GetComponent<ItemManager>().EventObjectThrow -= ResetMelee;
    }
    void Update()
    {
        CheckIfWeaponShouldAttack();
    }
    void SetInitialReferences()
    {
        myTransform = transform;
    }

    void CheckIfWeaponShouldAttack()
    {
        if (Time.timeScale > 0 && myTransform.root.CompareTag(GameManager.instance.ReturnPlayerTag()) && !isInUse)
            if (Input.GetButton("Fire1") && Time.time > nextSwing)
            {
                nextSwing = Time.time + swingRate;
                isInUse = true;
                CallEventPlayerInput();
            }
    }



    #region Audio
    void PlaySwingSound() //CalledbyAnimation
    {
        if (swingSound != null)
            AudioSource.PlayClipAtPoint(swingSound, myTransform.position, swingSoundVolume);
    }

    void PlayStrikeSound(Collision dummyCol, Transform dummyTransform)
    {
        if (strikeSound != null)
            AudioSource.PlayClipAtPoint(strikeSound, myTransform.position, strikeSoundVolume);
    }
    #endregion

    #region Swing
    void MeleeAttackActions()
    {
        myCollider.enabled = true;
        myRigidbody.isKinematic = false;
        myAnimator.SetTrigger("Attack");
    }
    void MeleeAttackCompleted()//Called by animation
    {
        myCollider.enabled = false;
        myRigidbody.isKinematic = true;
        isInUse = false;
    }
    #endregion

    void ResetMelee()
    {
        isInUse = false;
    }

    void SpawnHitEffect(Collision hitCollision, Transform hitTransform)
    {
        Quaternion quatAngle = Quaternion.LookRotation(hitCollision.contacts[0].normal);

        if (hitTransform.GetComponent<EnemyManager>() != null)
            Instantiate(enemyHitEffect, hitCollision.contacts[0].point, quatAngle);
        else
            Instantiate(defaultHitEffect, hitCollision.contacts[0].point, quatAngle);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag != GameManager.instance.ReturnPlayerTag() && isInUse && Time.time > nextSwingTime)
        {
            nextSwingTime = Time.time + swingRate;
            collision.transform.SendMessage("ProcessDamage", damage, SendMessageOptions.DontRequireReceiver);
            CallEventHit(collision, collision.transform);
        }
    }
}
