﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunManager : GunBase {

    private Animator myAnimator;
    private Transform myTransform;

    [Header("Ammo Information")]
    public string ammoName;
    public int clipSize;
    public int currentAmmo;
    public float reloadTime;

    [Header("Crosshair Information")]
    public GameObject crosshair;
    private Transform playerTransform;
    private Transform weaponCamera;
    private float playerSpeed;
    private float nextCaptureTime;
    private float captureInterval = 0.5f;
    private Vector3 lastPosition;
    public string weaponCameraName;

    [Header("Attack and Damage")]
    public int damage = 10;
    private float nextAttack;
    public float attackRate = 0.5f;

    [Header("Force Applied to Target")]

    public float forceToApply = 300;

    [Header("Effect Prefabs")]
    public GameObject defaultHitEffect;
    public GameObject enemyHitEffect;
    public GameObject muzzleFlash;

    [Header("Audio")]
    public float shootVolume = 0.4f;
    public float reloadVolume = 0.5f;
    public AudioClip shootSound;
    public AudioClip reloadSound;

    [Header("Input")]
    public bool isAutomatic;
    public string attackButtonName;
    public string reloadButtonName;

    [Header("Shooting")]
    private Transform camTransform;
    private RaycastHit hit;
    public float range = 400;
    private float offsetFactor = 7;
    private Vector3 startPosition;

    public LayerMask enemyLayer;

    void OnEnable()
    {
        EventPlayerInput += DeductAmmo;
        EventPlayerInput += CheckAmmoStatus;
        EventPlayerInput += PlayMuzzleFlash;
        EventPlayerInput += PlayShootAnimation;
        EventPlayerInput += PlayShootSound;
        EventPlayerInput += OpenFire;

        EventRequestReload += TryToReload;
        EventGunNotUsable += TryToReload;
        EventRequestGunReset += ResetGunReloading;
        EventAmmoChanged += UpdateAmmoUI;

        EventShotEnemy += ApplyDamage;
        EventShotEnemy += SpawnEnemyHitEffect;

        EventShotDefault += ApplyForce;
        EventShotDefault += SpawnDefaultHitEffect;

        EventSpeedCaptured += SetStartOfShootingPosition;

        this.GetComponent<ItemManager>().EventObjectThrow += ResetGun;

        PlayerManager.instance.EventAmmoChanged += UIAmmoUpdateRequest;

        SetInitialReferences();
        StartingSanityCheck();
        CheckAmmoStatus();
        CanvasCrosshairToggle();

        StartCoroutine(UpdateAmmoUIWhenEnabling());
    }

    void OnDisable()
    {
        EventPlayerInput -= DeductAmmo;
        EventPlayerInput -= CheckAmmoStatus;
        EventPlayerInput -= PlayMuzzleFlash;
        EventPlayerInput -= PlayShootAnimation;
        EventPlayerInput -= PlayShootSound;
        EventPlayerInput -= OpenFire;

        EventRequestReload -= TryToReload;
        EventGunNotUsable -= TryToReload;
        EventRequestGunReset -= ResetGunReloading;
        EventAmmoChanged -= UpdateAmmoUI;

        EventShotEnemy -= ApplyDamage;
        EventShotEnemy -= SpawnEnemyHitEffect;

        EventShotDefault -= ApplyForce;
        EventShotDefault -= SpawnDefaultHitEffect;

        EventSpeedCaptured -= SetStartOfShootingPosition;

        this.GetComponent<ItemManager>().EventObjectThrow -= ResetGun;
        PlayerManager.instance.EventAmmoChanged -= UIAmmoUpdateRequest;

        CanvasCrosshairToggle();
    }

    void Start()
    {
        SetInitialReferences();
        StartCoroutine(UpdateAmmoUIWhenEnabling());
    }

    void SetInitialReferences()
    {

        if (GetComponent<Animator>() != null)
            myAnimator = GetComponent<Animator>();

        myTransform = transform;
        camTransform = myTransform.parent;
        isGunLoaded = true; //So the player can attempt shooting right away.

        playerTransform = transform.parent;
        FindWeaponCamera(playerTransform);
        SetCameraOnDynamicCrosshairCanvas();
        SetPlaneDistanceOnDynamicCrosshairCanvas();

    }

    void Update()
    {
        CheckIfWeaponShouldAttack();
        CheckForReloadRequest();
        CapturePlayerSpeed();
        ApplySpeedToAnimation();
        CastRayForCrosshair();
    }

    #region Ammo
    void DeductAmmo()
    {
        currentAmmo--;
        UIAmmoUpdateRequest();
    }

    void TryToReload()
    {
        for (int i = 0; i < PlayerManager.instance.typesOfAmmunition.Count; i++)
        {
            if (PlayerManager.instance.typesOfAmmunition[i].ammoName == ammoName)
                if (PlayerManager.instance.typesOfAmmunition[i].ammoCurrentCarried > 0 && currentAmmo != clipSize && !isReloading)
                {
                    isReloading = true;
                    isGunLoaded = false;

                    if (myAnimator != null)
                        myAnimator.SetTrigger("Reload");
                    else
                        StartCoroutine(ReloadWithoutAnimation());
                }
            break;

        }
    }

    void CheckAmmoStatus()
    {
        if (currentAmmo <= 0)
        {
            currentAmmo = 0;
            isGunLoaded = false;
        }
        else if (currentAmmo > 0)
            isGunLoaded = true;

    }

    void StartingSanityCheck()
    {
        if (currentAmmo > clipSize)
            currentAmmo = clipSize;
    }

    void UIAmmoUpdateRequest()
    {
        for (int i = 0; i < PlayerManager.instance.typesOfAmmunition.Count; i++)
            if (PlayerManager.instance.typesOfAmmunition[i].ammoName == ammoName)
            {
                CallEventAmmoChanged(currentAmmo, PlayerManager.instance.typesOfAmmunition[i].ammoCurrentCarried);
                break;
            }

    }

    void ResetGunReloading()
    {
        isReloading = false;
        CheckAmmoStatus();
        UIAmmoUpdateRequest();
    }

    //Called by the reload animation
    public void OnReloadComplete()
    {
        for (int i = 0; i < PlayerManager.instance.typesOfAmmunition.Count; i++)
        {
            if (PlayerManager.instance.typesOfAmmunition[i].ammoName == ammoName)
            {
                int ammoTopUp = clipSize - currentAmmo;
                if (PlayerManager.instance.typesOfAmmunition[i].ammoCurrentCarried >= ammoTopUp)
                {
                    currentAmmo += ammoTopUp;
                    PlayerManager.instance.typesOfAmmunition[i].ammoCurrentCarried -= ammoTopUp;
                }
                else if (PlayerManager.instance.typesOfAmmunition[i].ammoCurrentCarried < ammoTopUp && PlayerManager.instance.typesOfAmmunition[i].ammoCurrentCarried != 0)
                {
                    currentAmmo += PlayerManager.instance.typesOfAmmunition[i].ammoCurrentCarried;
                    PlayerManager.instance.typesOfAmmunition[i].ammoCurrentCarried = 0;
                }
                break;
            }
        }
        ResetGunReloading();
    }

    IEnumerator ReloadWithoutAnimation()
    {
        yield return new WaitForSeconds(reloadTime);
        OnReloadComplete();
    }
    IEnumerator UpdateAmmoUIWhenEnabling()
    {
        yield return new WaitForSeconds(0.05f); //Factor to ensure that the UI is updated when changing weapons
        UIAmmoUpdateRequest();
    }

    void UpdateAmmoUI(int currentAmmo, int carriedAmmo)
    {
        if (CanvasManager.instance.currentAmmo != null)
            CanvasManager.instance.currentAmmo.text = currentAmmo.ToString();
        if (CanvasManager.instance.carriedAmmo != null)
            CanvasManager.instance.carriedAmmo.text = carriedAmmo.ToString();
    }
    #endregion

    #region Animator
    void PlayShootAnimation()
    {
        if (myAnimator != null)
            myAnimator.SetTrigger("Shoot");
    }
    #endregion

    #region Damage
    void ApplyDamage(Vector3 hitPosition, Transform hitTransform)
    {
        if (hitTransform.GetComponentInParent<EnemyManager>() != null)
            hitTransform.GetComponentInParent<EnemyManager>().ProcessDamage(damage);
    }
    #endregion

    #region Apply Force
    void ApplyForce(Vector3 hitPosition, Transform hitTransform)
    {
        if (hitTransform.GetComponent<Rigidbody>() != null)
            hitTransform.GetComponent<Rigidbody>().AddForce(-myTransform.forward * forceToApply, ForceMode.Impulse);
    }
    #endregion

    #region Hit Effects
    void SpawnDefaultHitEffect(Vector3 hitPosition, Transform hitTransform)
    {
        if (defaultHitEffect != null)
            Instantiate(defaultHitEffect, hitPosition, Quaternion.identity);
    }
    void SpawnEnemyHitEffect(Vector3 hitPosition, Transform hitTransform)
    {
        if (enemyHitEffect != null)
            Instantiate(enemyHitEffect, hitPosition, Quaternion.identity);
    }
    #endregion

    #region Muzzle Flash
    void PlayMuzzleFlash()
    {
        if (muzzleFlash != null)
            muzzleFlash.GetComponent<ParticleSystem>().Play();
    }
    #endregion

    #region Reset Gun When Thrown
    void ResetGun()
    {
        CallEventRequestGunReset();
    }
    #endregion

    #region Audio
    void PlayShootSound()
    {
        AudioSource.PlayClipAtPoint(shootSound, myTransform.position, shootVolume);
    }
    void PlayReloadSound()//Called by the reload animation event
    {
       AudioSource.PlayClipAtPoint(reloadSound, myTransform.position, reloadVolume);
    }
    #endregion

    #region Input
    void CheckIfWeaponShouldAttack()
    {
        if (Time.time > nextAttack && Time.timeScale > 0 && myTransform.root.CompareTag(GameManager.instance.ReturnPlayerTag()))
            if (Input.GetButtonDown(attackButtonName))
                AttemptAttack();
    }
    void AttemptAttack()
    {
        nextAttack = Time.time + attackRate;

        if (isGunLoaded)
            CallEventPlayerInput();
        else
            CallEventGunNotUsable();
    }
    void CheckForReloadRequest()
    {
        if (Input.GetButtonDown(reloadButtonName) && Time.timeScale > 0 && myTransform.root.CompareTag(GameManager.instance.ReturnPlayerTag()))
           CallEventRequestReload();
    }
    #endregion

    #region Shooting
    void OpenFire()
    {
        if (Physics.Raycast(camTransform.TransformPoint(startPosition), camTransform.forward, out hit, range))
        {
            CallEventShotDefault(hit.point, hit.transform);
            if (hit.transform.CompareTag(GameManager.instance.ReturnEnemyTag()))
               CallEventShotEnemy(hit.point, hit.transform);
        }
    }

    void SetStartOfShootingPosition(float playerSpeed)
    {
        float offset = playerSpeed / offsetFactor;
        startPosition = new Vector3(Random.Range(-offset, offset), Random.Range(-offset, offset), 1);
    }

    #endregion

    #region Dynamic Crosshair
    void CapturePlayerSpeed()
    {
        if (Time.time > nextCaptureTime)
        {
            nextCaptureTime = Time.time + captureInterval;
            playerSpeed = (playerTransform.position - lastPosition).magnitude / captureInterval;
            lastPosition = playerTransform.position;
            CallEventSpeedCaptured(playerSpeed);
        }
    }

    void ApplySpeedToAnimation()
    {
        crosshair.GetComponent<DynamicCrosshair>().SpeedChange(playerSpeed);
    }

    void FindWeaponCamera(Transform transformToSearchThrough)
    {
        if (transformToSearchThrough != null)
        {
            if (transformToSearchThrough.name == weaponCameraName)
            {
                weaponCamera = transformToSearchThrough;
                return;
            }

            foreach (Transform child in transformToSearchThrough)
                FindWeaponCamera(child);
        }
    }

    void SetCameraOnDynamicCrosshairCanvas()
    {
        if (crosshair != null && weaponCamera != null)
            crosshair.GetComponent<Canvas>().worldCamera = weaponCamera.GetComponent<Camera>();
    }

    void SetPlaneDistanceOnDynamicCrosshairCanvas()
    {
        if (crosshair != null)
            crosshair.GetComponent<Canvas>().planeDistance = 1;
    }

    void CanvasCrosshairToggle()
    {
        crosshair.SetActive(!crosshair.activeSelf);
    }

    void CastRayForCrosshair()
    {
        if (Physics.SphereCast(PlayerManager.instance.rayTransformPivot.position, 1, PlayerManager.instance.rayTransformPivot.forward, out hit, 5, enemyLayer))
            crosshair.GetComponent<DynamicCrosshair>().EnemyInSight(true);
        else
            crosshair.GetComponent<DynamicCrosshair>().EnemyInSight(false);
    }
    #endregion










}
