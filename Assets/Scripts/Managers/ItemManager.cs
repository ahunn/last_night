﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManager : ItemBase
{
    public string itemName;

    [Header("Rigidbodies")]
    public bool hasRigidbody = false;

    [Header("Throw")]
    public bool canBeThrown = true;
    public float throwForce = 20;
    public float mass = 5;
    private Vector3 throwDirection;
    //Add can be dropped later

    [Header("Set Local")]
    public Vector3 itemLocalPosition;
    public Vector3 itemLocalRotation;

    [Header("UI")]
    public bool hasUI = false;
    public GameObject itemUI;

    [Header("Audio")]
    public float defaultVolume;
    public AudioClip throwSound;
    //Expand on this later

    [Header("Colliders")]
    public PhysicMaterial myPhysicsMaterial;
    //Multiple colliders

    [Header("SetLayers")]
    public string throwLayer;
    public string pickupLayer;

   // [Header("Animator")]
    private Animator myAnimator;
    void Awake()
    {
        base.SetInitalReferences();
        transform.name = itemName;
        transform.tag = "Item";
        if (GetComponent<Animator>() != null)
            myAnimator = GetComponent<Animator>();
    }
    void OnEnable()
    {
        if (hasRigidbody)
        {
            EventObjectThrow += SetIsKinematicToFalse;
            EventObjectPickup += SetIsKinematicToTrue;
        }

        if (hasUI)
        {
            EventObjectPickup += EnableMyUI;
            EventObjectThrow += DisableMyUI;
        }

        EventObjectThrow += EnableColliders;
        EventObjectPickup += DisableColliders;

        EventObjectPickup += SetPositionOnPlayer;
        EventObjectThrow += PlayThrowSound;

        EventObjectPickup += SetItemToPickupLayer;
        EventObjectThrow += SetItemToThrowLayer;

        EventObjectThrow += DisableMyAnimator;
        EventObjectPickup += EnableMyAnimaor;

        EventPickupAction += CarryOutPickupActions;
        EventObjectPickup += SetRotationOnPlayer;
    }
    void OnDisable()
    {
        if (hasRigidbody)
        {
            EventObjectThrow -= SetIsKinematicToFalse;
            EventObjectPickup -= SetIsKinematicToTrue;
        }

        if (hasUI)
        {
            EventObjectPickup -= EnableMyUI;
            EventObjectThrow -= DisableMyUI;
        }
        EventObjectThrow -= EnableColliders;
        EventObjectPickup -= DisableColliders;

        EventObjectPickup -= SetPositionOnPlayer;
        EventObjectThrow -= PlayThrowSound;

        EventObjectPickup -= SetItemToPickupLayer;
        EventObjectThrow -= SetItemToThrowLayer;

        EventObjectThrow -= DisableMyAnimator;
        EventObjectPickup -= EnableMyAnimaor;

        EventPickupAction -= CarryOutPickupActions;
        EventObjectPickup -= SetRotationOnPlayer;
    }

    void Start()
    {
        CheckIfStartsInInventory();
        SetLayerOnEnable();
        SetPositionOnPlayer();
        SetRotationOnPlayer();
    }

    void Update()
    {
        CheckForThrowInput();
        CheckForDropInput();
    }

    void SetPositionOnPlayer()
    {
        if (transform.root.CompareTag(GameManager.instance.ReturnPlayerTag()))
            transform.localPosition = itemLocalPosition;
    }

    void SetRotationOnPlayer()
    {
        if (transform.root.CompareTag(GameManager.instance.ReturnPlayerTag()))
            transform.localEulerAngles = itemLocalRotation;
    }

    void CheckIfStartsInInventory()
    {
        if (transform.root.CompareTag(GameManager.instance.ReturnPlayerTag()))
        {
            SetIsKinematicToTrue();
            DisableColliders();
        }
            
    }

    #region Rigidbodies
    void SetIsKinematicToTrue(){ ReturnRigidbody().isKinematic = true;}

    void SetIsKinematicToFalse(){ ReturnRigidbody().isKinematic = false;}
    #endregion

    #region Throw
    void CheckForThrowInput()
    {
            if (Input.GetKeyUp(GameManager.instance.KeyboardReferences[GameManager.KeyHit.THROW]) && Time.timeScale > 0 && canBeThrown &&
                    ReturnTransform().root.CompareTag(GameManager.instance.ReturnPlayerTag()))
                CarryOutThrowActions();
    }
    void CarryOutThrowActions()
    {
        throwDirection = ReturnTransform().parent.forward;
        ReturnTransform().parent = null;

        if (ReturnTransform().gameObject.GetComponent<GunManager>() != null)
            ReturnTransform().gameObject.GetComponent<GunManager>().enabled = false;

        CallEventObjectThrow();
        HurlItem();
    }
    void HurlItem()
    {
        ReturnRigidbody().AddForce(throwDirection * throwForce, ForceMode.Impulse);
    }
    #endregion

    #region Drop
    void CheckForDropInput()
    {
        if (Input.GetKeyUp(GameManager.instance.KeyboardReferences[GameManager.KeyHit.THROW]) && Time.timeScale > 0 &&
            ReturnTransform().root.CompareTag(GameManager.instance.ReturnPlayerTag()))
        {
            ReturnTransform().parent = null;
            CallEventObjectThrow();
        }
    }
    #endregion

    #region UI
    void EnableMyUI()
    {
        if (itemUI != null)
            itemUI.SetActive(true);
    }
    void DisableMyUI()
    {
        if (itemUI != null)
            itemUI.SetActive(false);
    }
    #endregion

    #region Audio
    void PlayThrowSound()
    {
        if (throwSound != null)
            AudioSource.PlayClipAtPoint(throwSound, transform.position, defaultVolume);
    }
    #endregion

    #region Colliders
    void EnableColliders()
    {
        ReturnCollider().enabled = true;
        if (myPhysicsMaterial != null)
            ReturnCollider().material = myPhysicsMaterial;
              
    }
    void DisableColliders()
    {
        ReturnCollider().enabled = false;
    }
    #endregion

    #region Set Layer
    void SetItemToThrowLayer()
    {
        SetLayer(transform, throwLayer);
    }
    void SetItemToPickupLayer()
    {
        SetLayer(transform, pickupLayer);
    }
    void SetLayerOnEnable()
    {
        if (pickupLayer == "")
            pickupLayer = "Item";
        if (throwLayer == "")
            throwLayer = "Item";
        if (transform.root.CompareTag(GameManager.instance.ReturnPlayerTag()))
            SetItemToPickupLayer();
        else
            SetItemToThrowLayer();
    }
    void SetLayer(Transform tForm, string itemLayerName)
    {
        tForm.gameObject.layer = LayerMask.NameToLayer(itemLayerName);
        foreach (Transform child in tForm)
            SetLayer(child, itemLayerName);
    }
    #endregion

    #region Animator
    void EnableMyAnimaor()
    {
        if (myAnimator != null)
            myAnimator.enabled = true;
    }
    void DisableMyAnimator()
    {
        if (myAnimator != null)
            myAnimator.enabled = false;
    }
    #endregion

    #region Pickup
    void CarryOutPickupActions(Transform tParent)
    {
        if (ReturnTransform().gameObject.GetComponent<GunManager>() != null)
            ReturnTransform().gameObject.GetComponent<GunManager>().enabled = true;
        transform.SetParent(tParent);
        CallEventObjectPickup();
        transform.gameObject.SetActive(false);
    }

    #endregion








}
