﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class CanvasManager : MonoBehaviour {

    public GameObject menu;
    public GameObject gameOverOverlay;
    public GameObject inventory;
    public GameObject controlPanel;
    public GameObject objectivePanel;

    [Header("Player Canvas Elements")]
    public GameObject healthPrefab;
    private Text healthPrefabText;
    public GameObject playerHurtOverlay;

    [Header("Ammo Canvas Elements")]
    public GameObject ammoPrefab;
    public TextMeshProUGUI currentAmmo;
    public TextMeshProUGUI carriedAmmo;

    public static CanvasManager instance;


	void Awake () {
        instance = this;
        menu.SetActive(false);
        gameOverOverlay.SetActive(false);
        inventory.SetActive(false);
        controlPanel.SetActive(true);
        objectivePanel.SetActive(false);
        playerHurtOverlay.SetActive(false);

        healthPrefabText = healthPrefab.GetComponentInChildren<Text>();
    }
	

    public void ToggleMenu(){ menu.SetActive(!menu.activeSelf); }
    public void ToggleGameOver() { gameOverOverlay.SetActive(!gameOverOverlay.activeSelf); }
    public void ToggleInventory() { inventory.SetActive(!inventory.activeSelf); }

    public void ToggleControls()
    {
        if (!controlPanel.activeSelf)
        {
            controlPanel.SetActive(true);
            objectivePanel.SetActive(false);
        }
    }

    public void ToggleObjective()
    {
        if (!objectivePanel.activeSelf)
        {
            controlPanel.SetActive(false);
            objectivePanel.SetActive(true);
        }
    }

    public void UpdateHealth(int val)
    {
        healthPrefabText.text = val.ToString();
    }

    public void HurtEffect()
    {
        Sequence hurtOverlay = DOTween.Sequence();
        hurtOverlay.Append(playerHurtOverlay.GetComponent<Image>().DOColor(new Color(1, 0, 0, 0.5f), 0.01f))
                            .Append(playerHurtOverlay.GetComponent<Image>().DOColor(new Color(1, 0, 0, 0f), 2f));
        if (playerHurtOverlay != null)
        {
            //hurtOverlay.Kill();
            playerHurtOverlay.SetActive(true);
            hurtOverlay.Play().OnComplete(() => playerHurtOverlay.SetActive(false));
        }
    }

    public void ToggleAmmo() { ammoPrefab.SetActive(!ammoPrefab.activeSelf); }

}
