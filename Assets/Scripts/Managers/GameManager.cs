﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : ManagerBase {

    public static GameManager instance;
    Dictionary<string, EventTriggered> EventsTriggered;

    private bool isGameOver = false;
    private bool isInventoryUIOn = false;
    private bool isPaused = false;

    public enum KeyHit { MENU, INVENTORY, PICKUP, THROW, RELOAD};
    public Dictionary<KeyHit, KeyCode> KeyboardReferences = new Dictionary<KeyHit, KeyCode>();

    //To change references within engine
    public string playerTag;
    public string enemyTag;

    public GameObject playerPrefab;

	void Awake ()
    {
        instance = this;
        EventsTriggered = new Dictionary<string, EventTriggered>();
        SetInitialReferences();
    }
    void Update()
    {
        MenuKeyState();
        InventoryKeyState();
    }

    void SetInitialReferences()
    {
        KeyboardReferences[KeyHit.MENU] = KeyCode.Escape;
        KeyboardReferences[KeyHit.INVENTORY] = KeyCode.I;
        KeyboardReferences[KeyHit.PICKUP] = KeyCode.E;
        KeyboardReferences[KeyHit.THROW] = KeyCode.Q;
        KeyboardReferences[KeyHit.RELOAD] = KeyCode.R;
    }

    public string ReturnPlayerTag()
    {
        return (playerTag == "") ? "Player" : playerTag;
    }
    public string ReturnEnemyTag()
    {
        return (playerTag == "") ? "Enemy" : enemyTag;
    }

    public void ToggleMenu()
    {
        CanvasManager.instance.ToggleMenu();
        PlayerManager.instance.ToggleFirstPersonController();
        TogglePause();
        ToggleCursor();
    }
    public void ToggleInventory()
    {
        CanvasManager.instance.ToggleInventory();
        PlayerManager.instance.ToggleFirstPersonController();
        TogglePause();
        ToggleCursor();
        isInventoryUIOn = !isInventoryUIOn;
    }

    //Sub menus for pause menu
    public void ToggleControls()
    {
        if (isPaused)
            CanvasManager.instance.ToggleControls();
    }
    public void ToggleObjective()
    {
        if (isPaused)
            CanvasManager.instance.ToggleObjective();
    }

    public void TogglePause()
    {
        Time.timeScale = isPaused ? 1 : 0;
        isPaused = !isPaused;
    }
    public void ToggleCursor()
    {
        Cursor.visible = isPaused? true : false;
        Cursor.lockState = Cursor.visible ? CursorLockMode.None : CursorLockMode.Locked;
    }

    void MenuKeyState()
    {
        if (Input.GetKeyUp(KeyboardReferences[KeyHit.MENU]) && !isGameOver && !isInventoryUIOn)
            ToggleMenu();
    }
    void InventoryKeyState()
    {
        if (Input.GetKeyUp(KeyboardReferences[KeyHit.INVENTORY]) && !isGameOver)
            ToggleInventory();
    }
}
