﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerManager : PlayerBase
{
    [Header("Health")]
    public int playerHealth;

    [Header("Detection")]
    public LayerMask layerToDetect;
    public Transform rayTransformPivot; // "What transform will the ray be fired from?"

    [System.Serializable]
    public class AmmoTypes
    {
        public string ammoName;
        public int ammoMaxQuantity;
        public int ammoCurrentCarried;

        public AmmoTypes(string aName, int aMaxQuantity, int aCurrentCarried)
        {
            ammoName = aName;
            ammoMaxQuantity = aMaxQuantity;
            ammoCurrentCarried = aCurrentCarried;
        }
    }
    [Header("Ammo Box")]
    public List<AmmoTypes> typesOfAmmunition = new List<AmmoTypes>();

    public static PlayerManager instance;
    private FirstPersonController playerController;
    private Transform itemAvailableForPickup;
    private RaycastHit hit;
    private float detectRange = 3;
    private float detectRadius = 0.9f;
    private bool itemInRange;
    private float labelWidth = 200;
    private float labelHeight = 50;

    void OnEnable()
    {
        EventPlayerHealthDeduction += PlayerDamaged;
        EventPlayerHealthIncrease += PlayerHealing;
        EventPickedUpAmmo += PickedUpAmmo;
        GetComponent<PlayerInventory>().enabled = true;
    }
    void OnDisable()
    {
        EventPlayerHealthDeduction -= PlayerDamaged;
        EventPlayerHealthIncrease -= PlayerHealing;
        EventPickedUpAmmo -= PickedUpAmmo;
    }

    void Awake () {
        instance = this;
        playerController = this.GetComponent<FirstPersonController>();
    }
    void Update()
    {
        CastRayForDetectingItems();
        CheckForItemPickupAttempt();
        
    }
    void OnGUI()
    {
        if (itemInRange && itemAvailableForPickup != null)
        {
            //Borderlands esque stats screen
            //GUI.DrawTexture(new Rect(Screen.width / 2 - 400 / 2, Screen.height / 2 - 400/2, 400, 400), test);
            GUI.Label(new Rect(Screen.width / 2 - labelWidth / 2, Screen.height / 2, labelWidth, labelHeight),
                   itemAvailableForPickup.name);



        }

    }

    #region Health 
    void PlayerDamaged(int val)
    {
        playerHealth -= val;
        CanvasManager.instance.HurtEffect();
        if (playerHealth <= 0)
        {
            playerHealth = 0;
            GameOver();
        }
        if (CanvasManager.instance.healthPrefab != null)
            CanvasManager.instance.UpdateHealth(playerHealth);
    }
    void PlayerHealing(int val)
    {
        playerHealth += val;
        if (playerHealth > 100)
            playerHealth = 100;

        if (CanvasManager.instance.healthPrefab != null)
            CanvasManager.instance.UpdateHealth(playerHealth);
    }
    void GameOver()
    {
        CanvasManager.instance.ToggleGameOver();
        GameManager.instance.TogglePause();
        GameManager.instance.ToggleCursor();
        ToggleFirstPersonController();
    }
    #endregion

    #region Detection
    void CastRayForDetectingItems()
    {
        if (Physics.SphereCast(rayTransformPivot.position, detectRadius, rayTransformPivot.forward, out hit, detectRange, layerToDetect))
        {
            itemAvailableForPickup = hit.transform;
            itemInRange = true;
        }
        else
            itemInRange = false;
    }
    void CheckForItemPickupAttempt()
    {
        if (Input.GetKeyUp(GameManager.instance.KeyboardReferences[GameManager.KeyHit.PICKUP]) && Time.timeScale > 0 &&
                itemInRange && itemAvailableForPickup.root.tag != GameManager.instance.ReturnPlayerTag())
        {

                itemAvailableForPickup.GetComponent<ItemManager>().CallEventPickupAction(rayTransformPivot);

            if (itemAvailableForPickup.GetComponent<GunManager>() != null)
                itemAvailableForPickup.GetComponent<GunManager>().enabled = true;
        }
    }
    #endregion

    #region Ammo
    public void PickedUpAmmo(string ammoName, int quantity)
    {
        for (int i = 0; i < typesOfAmmunition.Count; i++)
            if (typesOfAmmunition[i].ammoName == ammoName)
            {
                typesOfAmmunition[i].ammoCurrentCarried += quantity;

                if (typesOfAmmunition[i].ammoCurrentCarried > typesOfAmmunition[i].ammoMaxQuantity)
                    typesOfAmmunition[i].ammoCurrentCarried = typesOfAmmunition[i].ammoMaxQuantity;

                PlayerManager.instance.CallEventAmmoChanged();
                break;
            }
    }
    #endregion

    public void ToggleFirstPersonController() { playerController.enabled = !playerController.enabled; }
    



    
}
