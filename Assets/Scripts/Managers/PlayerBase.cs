﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerBase : MonoBehaviour {

    public delegate void PlayerHealthEventHandler(int healthChange);

    public event PlayerHealthEventHandler EventPlayerHealthDeduction;
    public event PlayerHealthEventHandler EventPlayerHealthIncrease;

    public delegate void AmmoPickupEventHandler(string ammoName, int quantity);
    public event AmmoPickupEventHandler EventPickedUpAmmo;

    public delegate void GeneralEventHandler();
    public event GeneralEventHandler EventInventoryChanged;
    public event GeneralEventHandler EventHandsEmpty;
    public event GeneralEventHandler EventAmmoChanged;


    public void CallEventAmmoChanged()
    {
        if (EventAmmoChanged != null)
            EventAmmoChanged();
    }

    public void CallEventPickedUpAmmo(string type, int amt)
    {
        if (EventPickedUpAmmo != null)
            EventPickedUpAmmo(type, amt);
    }

    public void CallEventHandsEmpty()
    {
        if (EventHandsEmpty != null)
            EventHandsEmpty();
    }

    public void CallEventInventoryChanged()
    {
        if (EventInventoryChanged != null)
            EventInventoryChanged();
    }

    public void CallEventPlayerHealthDeduction(int dmg)
    {
        if (EventPlayerHealthDeduction != null)
            EventPlayerHealthDeduction(dmg);
    }

    public void CallEventPlayerHealthIncrease(int increase)
    {
        if (EventPlayerHealthIncrease != null)
            EventPlayerHealthIncrease(increase);
    }


}
