﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace S3
{
	public class EndGameScript : MonoBehaviour {

private bool countOver = false;
public Text gameOverText;
public GameObject gameManager;
private string win = "You Win, For Now.";
		void Start()
		{
			StartCoroutine(Counter());
		}

		void OnTriggerEnter(Collider other)
		{
			if(other.tag == "Player")
			{
				if (countOver){
					gameManager.GetComponent<GameManager_Master>().CallEventGameOver();
				}
				else{
					gameManager.GetComponent<GameManager_Master>().CallEventGameOver();
					gameOverText.text = win;
				}
			}
		}

		IEnumerator Counter()
		{
			yield return new WaitForSeconds(120);
			 countOver = true;
		}
	}
}
