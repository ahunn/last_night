﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public class InventoryID : MonoBehaviour
{
    public int itemID;
    public string itemName;
    public string description;
    public int damage;
}
